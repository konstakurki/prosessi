#!/usr/bin/env python3
import sys
text = sys.stdin.read().replace('\n','')
decoded = [ord(c)-192 for c in text]
with open(sys.argv[1],'wb') as f:
    f.write(bytes(decoded))
