#!/usr/bin/env python3
import sys
import textwrap
with open(sys.argv[1],'rb') as f:
    text = ''.join([chr(b+192) for b in list(f.read())])
for line in textwrap.wrap(text,600):
    print(line)
