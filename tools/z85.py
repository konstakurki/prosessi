z85='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#'
z85enc = {}
z85dec = {}

for i in range(85):
    z85enc[i] = z85[i]
    z85dec[z85[i]] = i

def decode_frame(s):
    n = 0
    decoded = []
    for i in range(5):
        n += z85dec[s[4-i]]*85**i
    for i in range(4):
        m = n // 256**(3-i)
        n = n - 256**(3-i) * m
        decoded.append(m)
    return decoded

def decode(s):
    decoded = []
    s = [s[n:n+5] for n in range(0,len(s),5)]
    for i in s:
        decoded += decode_frame(i)
    return bytes(decoded)

def encode_frame(b):
    l = list(b)
    n = 0
    encoded = ''
    for i in range(4):
        n += l[3-i]*256**i
    for i in range(5):
        m = n // 85**(4-i)
        n = n - 85**(4-i) * m
        encoded += z85enc[m]
    return encoded

def encode(s):
    encoded = ''
    s = [s[n:n+4] for n in range(0,len(s),4)]
    for i in s:
        encoded += encode_frame(i)
    return encoded

def pad(b):
    n = len(b) % 4
    return b + (4-n)*b'\x00'

import sys

if sys.argv[1] == 'encode':
    with open(sys.argv[2],'rb') as f:
        data = f.read()
    encoded = encode(pad(data))
    N = 5*64
    for i in [encoded[n:n+N] for n in range(0,len(encoded),N)]:
        print(i)
elif sys.argv[1] == 'decode':
    with open(sys.argv[2],'r') as f:
        data = ''.join(f.read().split())
    with open(sys.argv[3],'wb') as f:
        f.write(decode(data))
