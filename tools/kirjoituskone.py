#!/usr/bin/env python3
import sys
with open('README.md','r') as f:
    text = f.readlines()
n = len(text) - int(sys.argv[1])
alku = text[:n]
loppu = text[n:]
for i in alku:
    print(i[:-1])
while True:
    inputline = input('> ')
    if inputline == 'exit()':
        break
    else:
        alku.append(inputline+'\n')
        updated = alku + loppu
        updated = ''.join(updated)
        n += 1
        with open('README.md','w') as f:
            f.write(updated)
