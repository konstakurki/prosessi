# <!--meta:BookTitle-->Prosessi<!--/-->—<!--meta:BookSubtitle-->Itseuskon jäljillä<!--/-->

<<'# Hei!'
<meta charset='utf-8' />
Tämä on kirjan _Prosessi—Itseuskon jäljillä_ lähdekoodi. Kaikki teksti on tässä tiedostossa, joten voit lukea kirjan yksinkertaisesti selaamalla alaspäin. Teknisistä syistä johtuen tämä versio voi olla pari pykälää sivulla <!--meta:BookURL-->http://prosessi.org<!--/--> olevaa versiota edellä. Ennen varsinaisen kirjan tekstiä on selostusta kirjan teknisestä toteutuksesta, joten jos tulit vain lukemaan viimeisintä versiota kirjasta, [hyppää suoraan esipuheeseen.](#esipuhe) Kirja on aikaisessa työvaiheessa; aloitin sen kirjoittamisen 23. helmikuuta 2019. Olen päättänyt, että se on valmis, kun teen tuhannennen commitin tähän repositoryyn.

* Kirjoittaja: <!--meta:BookAuthor-->Konsta Kurki<!--/-->
* Kirjoittajan puhelinnumero: <!--meta:AuthorPhoneNumber-->+358458864582<!--/-->
* Kirjoittajan kotisivu: <!--meta:AuthorURL-->http://konstakurki.org<!--/-->
* Lähdekoodi: <!--meta:SourceURL-->https://git.sr.ht/~konstakurki/prosessi<!--/-->
* Käännetty pdf (ei välttämättä uusin): <!--meta:CompiledURL-->http://prosessi.org/prosessi.pdf<!--/-->

&copy; <!--meta:CopyrightPeriod-->2019<!--/--> <!--meta:CopyrightHolder-->Konsta Kurki<!--/-->

<!--meta:LicenseName-->CC BY-SA 4.0<!--/--> (<!--meta:LicenseURL-->https://creativecommons.org/licenses/by-sa/4.0/<!--/-->)

## Tekniikkaa

Tämä tiedosto näyttänee äkkiseltään katsottuna pelkältä markdown-dokumentilta, mutta itseasiassa se on suoritettava ohjelma. `README.md`-tiedosto sisältää yksin kaiken lähdekoodin—tässä on markdownin lisäksi Bashia, Pythonia, LaTeXia, HTML:ää, CSS:ää, javascriptiä ja ASCII-merkkeihin enkoodattua binääridataa—eikä se viittaa mihinkään muuhun tiedostoon. Tästä ei ole mitään hyötyä; olen kirjoittanut kaiken yhteen tiedostoon vain huvin vuoksi. Melkein kaikki koodi on näkyvillä koodiblokkeina sourcehutin formatoimalla sivulla. Jos haluat nähdä lähdekoodin mahdollisimman puhtaana, ilman mitään muotoilua, katso [tänne](https://git.sr.ht/~konstakurki/prosessi/tree/master/README.md).

<!--(Viitatako `README.md`-tiedostoon jatkossa sanalla “tämä” vai “se”? Ensimmäinen tuntuisi loogisemmalta, mutta jälkimmäinen kuulostaa paremmalta. Annan korvan tehdä valinnan.)-->
Voit asentaa ohjelman yksinkertaisesti kloonaamalla tämän repositoryn.

```shell
$ git clone https://git.sr.ht/~konstakurki/prosessi
```
Ohjelman voi suorittaa terminaalissa (ainaking GNU/Linux-järjestelmissä; muista järjestelmistä en tiedä) yksinkertaisesti siirtymällä repon juureen (`$ cd prosessi/`) ja antamalla komennon

```shell
$ ./README.md
```
 Ohjelma tulostaa kirjaa ruudulle terminaali-ikkunan kokoinen sivu kerrallaan. Se formatoi tekstin kauniisti, tavuttaen ja tasoittaen rivit. Seuraavalle sivulle pääsee painamalla enteriä, edelliselle antamalla komennon `edellinen`. Kirjasta voi buildata HTML-, teksti-, LaTeX- ja pdf-versiot antamalla komennon `build`. Ohjelmasta pääsee pois antamalla komennon `lopeta`. Buildaaminen onnistuu myös komennolla

```shell
$ ./README.md build
```
ilman, että interaktiivinen kirja aukeaa. Ohjelma on ajettava repon juuressa, koska se kaivaa reposta metadataa, ja se vaatii toimiakseen Python 3.4:n. pdf-version buildaaminen vaatii lisäksi TeXliven.

Kirjan versionumero saadaan jakamalla commitin numero tuhannella, joten kirja on valmis sitten, kun teen tähän repoon commitin numero 1000. Pyrin buildaamaan ja uploadaamaan kirjasta uudet versiot kirjan nettisivulle jokaisen commitin jälkeen. Minulla ei ole automaattista systeemiä tähän. Tarvitsen buildaamiseen läppäriäni, joten jos työskentelen puhelimellani, eikä minulla ole läppäriä mukana, viivästyy kirjasivun päivittäminen. Tästä syystä tämä sourcehutin markdown-esikatselu voi olla pari askelta kirjan nettisivua edellä.

### Workflow

Elämääni hallitsee pyrkimys mennä siitä, missä aita on matalin. En välttämättä jaksa lämmittää ruokaa tai ottaa päivävaatteita yöksi pois. Sama ajatusmalli on myös tähän kirjaan liittyvien valintojen takana. Tietysti tämä ei voi aivan pitää paikkaansa, koska olen tehnyt paljon aivan turhaa työtä saadakseni kaiken lähdekoodin yhteen tiedostoon. Mutta silloinkin, kun saan päähäni jonkun aivan turhan päämäärän, pyrin saavuttamaan sen pienimmällä mahdollisella vaivalla.

Tämä asenne on johtanut siihen, että käytän tietokonetta pääasiassa terminaalin (komentorivin) kautta. Osaan vain välttämättömät komennot, ja tietokoneeni konfiguraatio poikkeaa hyvin vähän defaultista. Minulla on minimaalinen Debian GNU/Linux-asennus ilman graafista työpöytäympäristöä. Windowmanagerina minulla on evilwm. En tiedä, millaisia muut windowmanagerit ovat; en muista koskaan kokeilleeni mitään muuta windowmanageria.

Jaan terminaalini tmuxilla useampaan palaseen. En osaa juuri mitään tmux-komentoja. Tekstiä editoin Vimillä. Vim-komentojakaan en osaa juuri ollenkaan. Hallitsen projektieni versioita Gitillä. En osaa juurikaan Gittiä; brancheista en tiedä oikeastaan yhtään mitään. Suosin tekstiä/lähdekoodia ja wysiwym-paradigmaa. Jos koodaan jotain, koodaan luultavasti Pythonia, koska Pythonilla saa nopeasti tehtyä monenlaisia asioita, vaikka tietoa ja taitoa olisi hyvin vähän.

Kaikki tämä heijastuu tämän kirjan tekniseen toteutukseen. Kirja on jono merkkejä, jota muokkaan ja kehitän tekstieditorilla. Kirjoitan vain sellaista tekstiä ja koodia, jonka ymmärrän itse. Koodini on alkeellista ja epäeleganttia. Kirja asuu Git-repossa. Git-repo oleilee läppärilläni, puhelimessani ja sourcehutin servereillä.

Käytännössä kehitän kirjaa sillä tavalla, että editoin `README.md`-tiedostoa, ja sitten annan komennot

```bash
$ git add README.md
$ git commit -m 'Selostus siitä, mitä tuli muutettua'
$ git push
```
Voin tehdä tämän sekä läppärilläni että myös puhelimellani, sillä käytän Termux-nimistä appia, joka luo terminaaliympäristön puhelimeen. Siinä toimii Git, tmux, Vim ja Python, eli melkein kaikki tarvittava. Voin kirjoittaa vaeltaessani luonnon helmassa. Jos kirjoitan pelkkää suomea, on puhelimen onscreen-näppäimistö tarpeeksi hyvä Vimin käyttämiseen. Bluetooth-näppäimistön ja puhelimelle tarkoitetun pöytäjalan kanssa puhelimesta tulee suorastaan erinomainen työasema.

Kirjan buildaamiseen tarvitaan TeXlive, eikä sitä ole (käsittääkseni) saatavilla Termuxiin. Lisäksi Termuxissa Python antoi jonkun kummallisen Unicode-virheen tekstin sekaan enkoodamani kuvan kohdalla. Niinpä tarvitsen buildaamiseen läppäriä. Puhelin ei siis riitä kaikkeen, vaikka mekaaniselta suorituskyvyltään nykyaikainen älypuhelin on paljon parempi kuin vuosikymmenen ikäinen ThinkPad X200s-läppärini<!--, jonka metallirungossa on 3cm x 1.5cm kokoinen reikä-->. Tämä johtuu korporaatioylivallasta, joka nykyteknologian tapauksessa elää pitkälti siitä, että ihmiset hyväksyvät ei-vapaan/suljetun (engl. proprietary/closed) koodin ja salaiset/suljetut hardwarespesifikaatiot.

### Filosofiaa

<!--Tämän kirjan sisältö ja tekninen toteutus edustavat kahta ääripäätä ihmisajattelussa. Ne edustavat ääripäitä paitsi ajattelun laadussa, myös sen tärkeydessä: kirjan sisältö koskee kaikista tärkeintä asiaa maailmassa, ja kirjan tekninen toteutus taas on täysin turhaa runkkaamista.-->

Kirjan kirjoittaminen on ajatuksen muuttamista sanojen jonoksi. Käytännön kirjoitustyössa tulee vastaan käytännön ongelma: kuinka tallentaa tuo jono, ja kuinka jakaa sitä toisille ihmisille? Sanat voisi kirjoittaa paperille, jota sitten kierrättää lukjalta toiselle. Tässä on kuitenkin omat hankaluutensa. Nykypäivänä moni kirjoittaa sanansa fyysiseen todellisuuteen tietokoneella käyttäen monimutkaista tekstinkäsittelyohjelmaa, esimerkiksi erästä W-alkuista proprietaryhärpäkettä, ja jakaa lopputulosta esimerkiksi sähköpostilla. Tässäkin on käytännön hankaluutensa. Lisäksi tuo W-alkuinen härpäke on monella tapaa niin kauhea kapistus, että en itse halua koskea siihen edes kilometrin mittaisella tikulla.

Kolmas tapa on kirjoittaa tietokoneella puhdasta tekstiä—siis tekstiä, joka on vain jono merkkejä, ilman mitään muotoilua—ja antaa tietokoneen muuttaa tuo lähdeteksti sitten silmää miellyttävästi muotoilluksi tekstiksi esimerkiksi pdf-tiedostoon tai nettisivulle. Lähdeteksti on kirjoitettava tietysti jonkun tietokonekielen määrittelemiä sopimuksia noudattaen, jotta tietokone voi tehdä sille mitään järkevää. Itse olen valinnut tämän tavan.

Ehkä yksinkertaisin tapa julkaista muotoiltua tekstiä kirjoittaen lähdetekstiä on luoda repository sourcehutiin tai vastaavaan palveluun ja laittaa repoon `README.md`-tiedosto, jossa on tekstiä kirjoitettuna markdown-standardin mukaan. Tämä sivu on tehty juuri tällä tavalla. Mutta sourcehutin renderöimä markdown voisi näyttää paremmaltakin.

Sekosin päästäni ja vein tämän idean pidemmälle kuin tiedän kenenkään vieneen. Halusin, että minulla on tasan yksi tiedosto, joka ei vain sisällä kirjan tekstiä, vaan joka on _suoritettava ohjelma, joka luo kirjasta sekä Internet-selainten ymmärtämän elegantin nettisivun että paperilla kauniilta näyttävän pdf-tiedoston._ Halusin myös, että

* Koko tiedosto näyttää kauniilta sourcehutin formatoimana, ja että kaikki ohjelman funktionaalinen koodi on näkyvillä syntaksi korostettuna koodiblokeissa.
* Koodi on helppolukuista ja sitä on mahdollisimman vähän.
* Nettisivu on [validia HTML-koodia.](https://validator.w3.org/)
* Ohjelma toimii lisäksi interaktiivisena kirjana, joka tulostaa kirjaa terminaali-ikkunaan. Ohjelma tulostaa tekstin rivit tasattuna ja sanat tarvittaessa tavutettuna, jotta terminaalitulosteen visuaalinen ilme miellyttää silmää.
* Ohjelma riippuu mahdollisimman vähän ulkoisista kirjastoista tai ohjelmista. Niinpä päätin, että kaikki funktionaalisuus tulisi onnistua käyttäen Python 3.4:ä ja sen standardikirjastoa. Joudun poikkeamaan tästä sen verran, että metadataa kaivetaa reposta käyttäen Gittiä, pdf-tiedosto luodaan käyttäen pdflatexia, ja koko ohjelma on teknisesti ottaen bash-skripti, joka vain kutsuu Python-tulkkia.

Nämä seikat johtivat muunmuassa siihen, että minun oli kirjoitettava oma markdown-parseri. Se, että kirjailijan täytyy kirjoittaa kirjaansa parseri kuulostaa varmasti hullulta—varsinkin, kun kirjailija ei tässä tapauksessa ole edes mikään taitava ohjelmoija. Huomasin kuitenkin, että parserin sai tehtyä aika helposti. Otin hiukan inspiraatiota [täältä](https://blog.beezwax.net/2017/07/07/writing-a-markdown-compiler/).

Niin kuin arvata saattaa, ei parserini implementoi markdownista muuta kuin pienen osan: eri kokoiset otsikot, tekstikappaleet, blokkilainaukset ja kursivoidun tekstin. Tämä voi kuulostaa rajoittavalta, ja sitä se onkin. Onko se huono asia? Aluksi ajattelin, että kyllä, mutta sitten tajusin, että se on itseasiassa juuri niinkuin sen pitääkin olla. On hyvä asia, että tekstin muotoilu pysyy yksinkertaisena. Jos todella tarvitsen jotakin lisää—jos haluan laittaa kirjaani esimerkiksi linkkejä—niin sitten laajennan parseriani tarpeen mukaan. Linkit kuulostavat varmaankin aika perusjutulta, mutta en ole ollenkaan varma, haluanko niitä kirjaani. Materiaalini ei juuri viittaa mihinkään tiettyihin kohteisin, joihin voisi fiksusti osoittaa linkillä, ja linkit houkuttelevat klikkaamaan itseään, häiriten lukemista.

```
# Markdown-demonstraatio

Demonstroin tässä oman pienen markdown-versioni kaikkia
ominaisuuksia. Yllä on iso otsikko -tyyppinen paragraph.
Tämä on normaali tekstiparagraph. Paragrapheja erottaa
yksi tai useampi tyhjä rivi. Paragraphin voi halutessaan
jakaa useammalle riville, kuten teen tässä demossa
selkeyden vuoksi, mutta se vaikeuttaa editointia. On
helpompi kirjoittaa paragraph aina yhdeksi riviksi ja
antaa editorin softwrapata rivit kirjoitusvaiheesssa.

## Pienempi otsikko

> Tämä on blokkilainausparagraph. *Kursivoitu teksti*
kirjoitetaan kahden asteriskin väliin. Update: laitoin
kuitenkin tuen linkeille. Ne kirjoitetaan näin:
<http://prosessi.org>. Säädin CSS-tyylin sellaiseksi,
että linkkiä ei voi klikata eikä se erotu muusta tekstistä
mitenkään.

### Vielä pienempi otsikko

Otsikko muuttuu sitä pienemmäksi, mitä enemmän
risuaitoja on sen edessä.

% Tein lisäksi tuen kommenttikappaleille.

Tässä oli kaikki. Tämä riittänee minulle tätä kirjaa
varten.
```
Olen jäänyt koukkuun tämän kirjan tekniikan kehittämiseen, ja välillä minulle tulee siitä huono omatunto. Voisin käyttää aikani itse kirjankin kirjoittamiseen. Mutta tätä tehdessä vaatimattomat ohjelmointitaitoni karttuvat hurjasti ja pelkoni uusia konsepteja kohtaan pienenee. Olin esimerkiksi aina kammoksunut uusien objektiluokkien luomista Pythonissa, enkä ollut tehnyt sitä kertaakaan ennen kuin aloin kirjoittamaan parseria. Olin ajatellut, että se on niin vaikeaa, että siihen menee koko elämä ja terveys. Mutta sitten ajattelin, että ei tästä parserista tule mitään Pythonin vakio-objekteilla, ja niin päätin kokeilla uuden luokan määrittelyä. Koko hommaan meni ehkä kymmenen minuuttia. Tajusin, kuinka helposta asiasta oli kyse, ja kuinka paljon kivuttomammin olisin voinut tehdä monet asiat aikaisemmissa pikku projekteissani, jos olisin niissä uskaltautunut määrittelemään luokkia.

Kirjan substanssi on se sanojen jono, mikä tämän `README.md`-tiedoston loppuosasta löytyy, mutta käytännöllisessä mielessä kirja on muutakin. Tämä kirja on koko `README.md`-tiedosto. Mutta tuo tiedosto muuttuu ajan kanssa. Jos joku puhuu kirjasta *Prosessi—Itseuskon jäljillä*, mitä versiota hän tarkoittaa? Tästä tullaan Git-repositoryyn, jossa kirja asuu. Itse ajattelen, että tämä kirja on itseasiassa koko tuo repository versiohistorioineen päivineen.

Niinpä `README.md` olettaa sijaitsevansa repositoryssä. Se kaivaa sieltä metadataa: versionumeron se laskee jakamalla aikaisempien committien lukumäärän tuhannella ja päivämääräksi se kaivaa edellisen kommitin päivämäärän. Web-kirjan versionumerota klikkaamalla pääsee committia vastaavalle sourcehutin sivulle, joka näyttää diffin, eli sen, miten `README.md` (ja mahdolliset muut tiedostot) ovat sen ja edellisen commitin välillä muuttuneet. Gitin kryptografisiin hasheihin perustuva integriteetinvarmistussysteemi antaa minulle mielenrauhaa. Jos repon uusin commit id on se, mitä sen pitää olla, on myös koko repo varmasti sitä, mitä sen pitää olla.

Alkuperäinen [Motherfucking Website](https://www.motherfuckingwebsite.com/) ja muut [motherfucking websitet](https://duckduckgo.com/mothefucking+website) näyttävät mallia siitä, miten hyvä nettisivu tehdään. Samasta syystä valitsin [sourcehutin](https://sourcehut.org/) GitLabin ohi. GitHub vaikuttaa olevan jo niin syvästi korruptoitunut systeemi, että haluan kiertää sen joka tapauksessa kaukaa.

### Konehuoneessa

_HUOM! Selostukset voivat olla hieman jäljessä, eivätkä ne välttämättä vastaa koodia. Koodi taas on varmasti on the bleeding edge, koska tämän tiedoston koodiblokit määrittävät suoraan sen koodin, mitä `./README.md` ajaa._

`README.md` on bash-skripti. En kuitenkaan osaa bashia juuri ollenkaan, ja niinpä tämä tiedosto koostuu suurimmaksi osaksi bashille mitäänsanomattomasta tavarasta. Koska tässä oleva Bash on niin alkeellista, uskalsin ottaa shebang-rivin pois. Tiedosto näyttää nyt paremmalta sourcehutin sivulla. Toivottavasti tästä ei ole haittaa.

```shell
# Hei!
cat << '!EOC!' > extract.py
def extract(ext,tunniste=''):
    with open('README.md','r') as f:
        data = f.read()
    strb = '\n```{0}\n'.format(ext)
    if tunniste != '':
        strb = '<!--{}-->'.format(tunniste) + strb
    stre = '\n```\n'
    code = ''
    n = 0
    while True:
        n = data.find(strb)
        if n == -1:
            break
        else:
            m = data.find(stre,n)
            code += data[n+len(strb):m] + '\n\n'
            data = data[n+len(strb):]
    return code
def main():
    with open('build.py','w') as f:
        f.write(extract('python'))
if __name__ == '__main__':
    main()
!EOC!
python3 extract.py
<<'!EOC!'
```
Ylläoleva koodiblokki on ensimmäinen aktiivinen osa `README.md`-ohjelmaa. Siinäkään ei ole juurikaan bashia; se vain tallentaa pienen pinon Python-koodia ja sitten ajaa tuon koodin Python-tulkin kautta. Funktio `extract(kieli)` poimii tästä samaisesta markdown-tiedostosta kaiken `kieli`-koodin, ja koodiblokin tallentama `extract.py`-ohjelma poimii tästä kaiken Python-koodin erilliseksi `build.py`-tiedostoksi, joka sitten ajetaan aivan `README.md`-tiedoston lopussa.

Katsotaanpa koodia, joka kertyy `build.py`-tiedostoon. Alussa luonnollisesti importataan kirjastoja.

```python
from subprocess import check_output
from decimal import Decimal
import os
import sys
from extract import extract
```
Jälkimmäinen rivi importtaa tämän tiedoston tallentamasta `extract.py`-tiedostosta `extract`-funktion, jota `build.py` käyttää markdownin, LaTeXin ja muiden kielien eristämiseen tästä tiedostosta.

### Parseri

Koska tarvitsemani markdown-ominaisuuksia on niin vähän, ei parserinkaan tarvitse olla kovin suuri. Katselin [tästä](https://blog.beezwax.net/2017/07/07/writing-a-markdown-compiler/) hieman mallia, mutta tulin siihen tulokseen, että en rupea tekemään tästä mitään varsinaista compileria. Ensimmäinen syy on se, että se olisi varmaankin hyvin hankala ja työläs homma, ja toinen syy on, että markdown ei ole samalla tavalla tarkasti ja loogisesti määritelty kieli, kuten vaikkapa HTML on. Tämä ei ole markdownin vika, vaan ominaisuus: markdownin ensisijainen tarkoitus on olla helposti luettava ja kirjoitettava markup-kieli, ja intuitiiviset ratkaisut tuppaavat olemaan vaikeasti formalisoitavissa.

Oma ratkaisuni on, että parseriin sisääntuleva teksti muutetaan listaksi `paragraph`-objekteja.

```python
class paragraph:
    def __init__(self,type,content):
        self.content = process_paragraph(content)
        self.type = type
```
Tässä oli `paragraph`-luokan määrittely. Hyvin yksinkertaista. Kappaleella on tyyppi (esim. `'text'` tai `'quote'`) ja sisältö. Sisältöä luodessa kutsutaan `process_paragraph`-funktiota, jonka määrittelen seuraavaksi.

```python
def process_paragraph(text):
    elements = []
    if text[0] == '*':
        elements.append(parcontent('em',''))
    elif text[0] == '<':
        elements.append(parcontent('link',''))
    else:
        elements.append(parcontent('text',text[0]))
    for i in range(1,len(text)):
        if text[i] == '*' and elements[-1].type != 'link':
            if elements[-1].type == 'em':
                elements.append(parcontent('text',''))
            else:
                elements.append(parcontent('em',''))
        elif text[i] == '<':
            if elements[-1].type != 'link':
                elements.append(parcontent('link',''))
            else:
                elements[-1].append(text[i])
        elif text[i] == '>':
            if elements[-1].type == 'link':
                try:
                    elements.append(parcontent(elements[-2].type,''))
                except:
                    elements.append(parcontent('text',''))
            else:
                elements[-1].append(text[i])
        else:
            elements[-1].append(text[i])
    return elements
```
Tämä funktio ottaa sisäänsä tekstiä ja palauttaa listan `parcontent`-objekteja, jotka määrittelen kohta. `parcontent`-objektilla on tyyppi (teksti tai kursivoitu teksti) ja sisältö, joka on tekstiä. Funktio siis jakaa tekstin jonoksi objekteja, jotka merkkaavat joko tavallista tai kursivoitua tekstiä. Se lisää (`append`) jonon viimeiseen objektiin merkkejä, kunnes asteriski tulee vastaan. Sitten se lisää jonoon toisentyyppisen objektin ja alkaa lisätä merkkejä siihen. `parcontent`-luokka pitää vielä määritellä.

```python
class parcontent:
    def __init__(self,type,content):
        self.content = content
        self.type = type
    def append(self,string):
        self.content += string
```
Kokonaisuudessaan siis `paragraph`-objekti luodaan antamalla sille tyyppi ja tekstiä. Sen sisällöksi tulee jono `parcontent`-objekteja, jotka kuvaavat joko tavallista tai kursivoitua tekstiä.

Sitten tarvitaan vielä funktio, joka jakaa tekstin oikeantyyppisiin `paragraph`-objekteihin. Se tulee tässä.

```python
def parse(text):
    text = [p for p in text.split('\n\n') if p != '']
    for i in range(len(text)):
        if text[i][0] == '\n':
            text[i] = text[i][1:]
    for i in range(len(text)):
        p = paragraph('paragraph',text[i])
        if text[i].startswith('div '):
            p = paragraph('div',text[i][4:])
        if text[i] == '/div':
            p = paragraph('/div',' ')
        if text[i].startswith('> '):
            p = paragraph('quote',text[i][2:])
        for n in range(1,6):
            if text[i].startswith(n*'#'+' '):
                p = paragraph('h{}'.format(n),text[i][n+1:])
        if text[i].startswith('%'):
            p = paragraph('comment',text[i][1:])
        text[i] = p
    return text
```
Funktio jakaa tekstin tyhjien rivien kohdalta listaksi kappaleita. Kappaleen tyypin se määrittää sen mukaan, millä tavalla rivi alkaa.

Parseri ei ollut tämän kummoisempi laitos. Kappaleiden lista on tietysti vielä muutettava HTML:ksi tai johonkin muuhun muotoon, jotta se voidaan saada ihmisen luettavaksi. Tulemme tähän myöhemmin.

### Tavutus

Sitten tavutukseen. Alunperin ajattelin, että olisi siistiä, jos interaktiivinen konsolikirja osaisi tavuttaa ja tasata rivit printatessa konsoliin. Siistiä, joskin täysin turhaa. Sitten tajusin, että samalla vaivalla saan tavutuksen rakennettua myös html-kirjaan: selain osaa tavuttaa sanat, jos potentiaalisissa tavutuskohdissa on html-elementti `&shy;` tai Unicode-merkki `U+00AD` (soft hyphen).

Joten päätin implementoida suomenkielen tavutussäännöt koodiini. Kaverini huomautti, että eihän tällaista tarvitsisi itse tehdä—on olemassa esimerkiksi [libvoikko](https://www.puimula.org/htp/doc/python/libvoikko.html)-moduli—mutta ajattelin, että on siistimpää, jos väsään tavutuksen itse. Onhan tämä koko homma loppujenlopuksi täysin turhaa touhuaa. Tavutussääntöjä en sentään jaksanut järkeillä itse, joten [tsekkasin säännöt internetistä.](http://www.kielitoimistonohjepankki.fi/haku/tavutus/ohje/153)

Aluksi määritellään hiukan suomenkieleen liittyviä stringejä: vokaalit, konsonantit, diftongit (en tiedä onko tuplavokaalit teknisesti ottaen diftongeja, mutta niiden suhteen tavutussääntö on sama: _ei_ tavuteta keskeltä) ja erisnimet, joita ei kai ole tapana tavuttaa ollenkaan. Lisäksi `tavutusta`-muuttuja määrittää joillekin sanoille pakotetun tavujaon.

```python
vok = 'aeiouyäöå'
kons = 'bcdfghjklmnpqrstvwxz'
dift = 'ai ei oi äi öi ey äy öy au eu ou ui yi iu iy ie uo yö'.split()
dift += [v+v for v in vok]

tavutusta = '''
osu-maa ole-vil-la ke-hit-tyä
'''.split()

tavu_dict = {s.replace('-',''):s for s in tavutusta}

names="""Aada Aamu Aava Aija Aila Aili Aina Aini Ainikki Aino Aira Airi
Aleksandra Aliisa Alina Alisa Alli Alma Amalia Amanda Anelma Anette
Anita Anitta Anja Anna Anne Anneli Anni Anniina Annika Annikki Annu
Annukka Ansa Arja Armi Asta Auli Aulikki Aune Auni Aura Auri Aurora
Birgitta Eerika Eeva Eevi Eija Eila Eine Eini Eira Elena Eleonoora
Eliisa Elina Elisa Elisabet Elise Ella Ellen Elli Elma Elmi Elna Elsa
Else Elsi Elvi Elviira Emilia Emma Emmi Enna Enni Erja Essi Ester
Esteri Eveliina Fanni Floora Hanna Hanne Hannele Heidi Heini Helena
Helga Heli Helinä Heljä Helka Helle Hellevi Helli Hellin Hellä Helmi
Helvi Hely Henna Henni Henriikka Hertta Heta Hilda Hilja Hilkka Hilla
Hille Hillevi Hilma Hilppa Iida Iina Iines Iiris Iisa Ilma Ilmatar
Ilmi Ilona Ilta Immi Impi Inari Inka Inkeri Irene Irina Irja Irma
Irmeli Isabella Isla Jaana Jade Janette Janika Janina Janita Janna
Janni Jasmin Jatta Jemina Jenna Jenni Joanna Johanna Jonna Josefiina
Julia Juliaana Jutta Juuli Kaarina Kaija Kaino Kaisa Kaisla Kaisu
Kanerva Karita Karoliina Kastehelmi Katariina Kati Katja Katri
Katriina Kerttu Kerttuli Kielo Kiia Kiira Kirsi Kirsti Klaara Krista
Kristiina Kukka Kylli Kyllikki Lahja Laila Laina Lauha Laura Leea
Leena Leeni Leila Lemmikki Lempi Lenita Liina Liisa Liisi Lilja Lilli
Linda Linnea Lotta Loviisa Lumi Lyydia Lyyli Maaret Maaria Maarit
Maija Maiju Maikki Maila Maili Mailis Maini Maire Maisa Manta
Margareeta Mari Maria Marianna Marianne Marika Marita Maritta Marja
Marjaana Marjatta Marjo Marjukka Marjut Marketta Martta Matilda
Matleena Meeri Melina Melissa Meri Merja Mervi Mette Mielikki Miia
Miina Miisa Mikaela Mila Milja Milka Milla Mimmi Mimosa Minea Minja
Minna Minttu Mira Mirella Mirja Mirjami Mirka Mirkka Mirva Moona Nanna
Nadja Neea Nella Nelli Netta Niina Nina Ninni Nita Noora Oili Oivi
Olga Olivia Onerva Oona Orvokki Outi Paula Pauliina Peppi Petra Pihla
Piia Pilvi Pinja Pipsa Pirita Piritta Pirjo Pirkko Pulmu Päivi
Päivikki Päivä Pälvi Raakel Raija Raila Raili Raisa Raita Rauha Rauna
Rauni Rebekka Reeta Reetta Reija Riia Riikka Riina Riitta Rita Ritva
Ronja Roosa Rosa Ruusu Ruut Saaga Saana Saara Saija Saila Saima Saimi
Saini Salla Salli Salme Sandra Sanelma Sanna Sanni Sara Sari Sarita
Satu Seela Seija Selja Selma Senja Senni Signe Siiri Silja Silva Sini
Sinikka Sirja Sirkka Sirkku Sirpa Siru Sisko Sivi Sofia Sohvi Soila
Soile Soili Sointu Solja Sonja Sorja Stella Suoma Suometar Susanna
Susanne Suvi Sylvi Sylvia Säde Taija Taika Taimi Taina Talvikki Tanja
Tarja Taru Teija Tellervo Teresa Terhi Terhikki Terttu Tessa Tiia
Tiina Tilda Tinja Titta Toini Tuija Tuire Tuomi Tuovi Tuula Tuuli
Tuulia Tuulikki Tytti Tyyne Tyyni Ulla Ulpu Unelma Ursula Valma
Valpuri Vanamo Vanessa Vappu Varma Varpu Vaula Veera Vellamo Venla
Verna Viena Vieno Viivi Vilhelmiina Vilja Vilma Viola Virpi Virva
Virve Vuokko Aadolf Aapeli Aapo Aappo Aarne Aarni Aarno Aaro Aaron
Aarre Aarto Aatami Aatos Aatto Aatu Ahti Ahto Ahvo Aimo Akseli Albert
Aleksanteri Aleksi Aleksis Alfred Allan Alpi Alpo Altti Alvar Alvi
Anselmi Anssi Antero Anton Antti Antto Anttoni Arhippa Arho Armas Arsi
Arto Arttu Artturi Arvi Arvo Asko Aslak Asmo Asser Atro Atso Atte
August Aukusti Aulis Auno Auvo Benjamin Daavid Daniel Edvard Eeli
Eelis Eemeli Eemi Eemil Eerik Eerikki Eero Eetu Eevert Einari Eino
Elias Eljas Elmer Elmeri Elmo Ensio Erkka Erkki Ernesti Erno Esaias
Esko Frans Gabriel Hannes Hannu Hans Harri Heikki Heimo Heino Hemminki
Hemmo Henri Henrik Henrikki Herkko Herman Hermanni Hugo Iikka Iiro
Iisakki Iivari Iivo Ilari Ilkka Ilmari Ilmo Ilpo Ilppo Immanuel Immo
Into Isko Ismo Isto Jaakkima Jaakko Jaakob Jaakoppi Jalmari Jalo Jami
Jani Janne Jari Jarkko Jarmo Jarno Jasper Jere Jeremias Jesper Jesse
Jimi Jiri Joakim Joel Johannes Joni Jonne Jonni Jooa Joona Joonas
Joonatan Joose Joosef Jooseppi Jori Jorma Jouko Jouni Jousia Juha
Juhana Juhani Juho Jukka Julius Jussi Justus Juuso Jyri Jyrki Kaapo
Kaappo Kaapro Kaarle Kaarlo Kalervo Kaleva Kalevi Kalle Kari Karri
Kasper Kasperi Kauko Kauno Keijo Keimo Kerkko Kimi Kimmo Klaus Konsta
Konstantin Kosti Kristian Kuisma Kullervo Kustaa Kustavi Kyösti Lari
Lasse Lassi Launo Lauri Leevi Lenni Luka Luukas Lyly Mainio Manne Manu
Markku Marko Markus Martti Matias Matti Mauno Maunu Mauri Mies Miika
Miikka Mika Mikael Mikko Miko Milo Miro Miska Nestori Niilo Niki
Niklas Niko Nikodemus Nikolai Nooa Noel Nuutti Nyyrikki Ohto Oiva Okko
Olavi Oliver Olli Onni Orvo Oskari Osmo Ossi Ossian Otso Otto Paavali
Paavo Panu Pasi Patrik Paul Pauli Paulus Peetu Pekka Pekko Pellervo
Pentti Pertti Perttu Petri Petteri Pietari Pirkka Pyry Päiviö Päivö
Raafael Raimo Raine Rainer Raino Rami Rasmus Rauli Rauno Reijo Reima
Reino Reko Rikhard Riku Risto Robert Robin Roni Roope Sakari Saku
Salomo Salomon Sami Sampo Sampsa Samu Samuel Samuli Santeri Santtu
Sasu Saul Sauli Sebastian Seppo Severi Silvo Simo Sipi Sippo Sisu
Soini Sulevi Sulho Sulo Sylvester Taavetti Taavi Tahvo Taisto Taito
Taneli Tapani Tapio Tarmo Tarvo Tatu Tauno Teemu Teijo Tenho Teppo
Terho Terno Tero Teuvo Tiitus Timi Timo Tino Toimi Toivo Tomi Tommi
Toni Topi Topias Torsti Touko Tuomas Tuomo Turkka Turo Tuukka Tuure
Ukko Uljas Untamo Unto Uolevi Uoti Urho Urmas Urpo Usko Uuno Valdemar
Valentin Valio Valo Valto Valtteri Veeti Veijo Veikka Veikko Veini
Veli Verneri Vertti Vesa Vihtori Vilhelm Vilho Vili Viljami Viljo
Ville Vilppu Visa Voitto Väinämö Väinö Ylermi Yrjänä Yrjö""".split()
```
Seuraava funktio tarkistaa, alkaako sana jollakin erisnimellä vai ei.

```python
def startswithaname(sana):
    output = False
    for n in names:
        if sana.startswith(n):
            output = True
            break
    return output
```
Seuraava funktio hoitaa tavutuksen. Se ottaa sisäänsä sanan ja palauttaa sen muokattuna siten, että jokaiseen tavutuspisteeseen on lisätty soft hyphen -merkki.

```python
def tavuta(sana):
    if sana in metadata:
        return sana
    elif sana in tavu_dict:
        return tavu_dict[sana].replace('-','\u00ad')
    elif startswithaname(sana):
        return sana
    else:
        sana = list(sana)
        for i in range(len(sana)-1):
            if sana[i] + sana[i+1] not in dift and sana[i] in vok and sana[i+1] in vok:
                sana[i] = sana[i] + '\u00ad'
        for i in range(len(sana)-1):
            if sana[i] in kons and sana[i+1] in vok:
                sana[i] = '\u00ad' + sana[i]
        sana[0] = sana[0][-1]
        sana = ''.join(sana)
        if len(sana) > 1:
            if sana[1] == '\u00ad':
                sana = sana [0] + sana[2:]
            if sana[-2] == '\u00ad':
                sana = sana [:-2] + sana[-1]
        return sana

def tavutasanat(text):
    if text == '':
        return ''
    else:
        words = []
        t1 = text[0].isalpha
        words = [text[0]]
        for c in text[1:]:
            t2 = c.isalpha()
            if t1 == t2:
                words[-1] += c
            else:
                words.append(c)
                t1 = t2
        for i in range(len(words)):
            if words[i].isalpha():
                words[i] = tavuta(words[i])
        return ''.join(words)
```
Ensimmäiset kolme vaihtoehtoa hoitavat erikoistapauksia, ja viimeinen vaihtoehto (`else`) implementoi varsinaiset tavutussäännöt. Ensimmäinen `for`-luuppi laittaa tavuviivan kahden peräkkäisen vokaalin väliin, mikäli vokaalipari ei ole `dift`-listassa. Toinen `for`-luuppi taas laittaa tavuviivan jokaisen sellaisen konsonantin eteen, jota seuraa vokaali. Lopussa poistetaan sellaiset tavuviivat, jotka irrottavat yksittäisen kirjaimen sanan alusta tai lopusta. Tavutus toimii aika hyvin, kuten kirjan nettisivua selaamalla voi havaita.

### LaTeX ja HTML

Ja sitten buildaamaan sivuja. Tälläerää en jaksa kirjoittaa toimintaselostusta pidemmälle.

```python
def ident(string):
    rep = {'\u00ad':'',' ':'-','ä':'a','ö':'o','å':'a','—':'-'}
    string = string.lower()
    for key in rep:
        string = string.replace(key,rep[key])
    return string

def html(document):
    htmlnames = {'h1':'h1','h2':'h2','h3':'h3','h4':'h4','paragraph':'p','em':'em','quote':'blockquote'}
    n = 0
    m = 0
    toc = []
    output = ''
    for i in document:
        o = ''
        for j in i.content:
            if j.type == 'text':
                o += tavutasanat(j.content)
            elif j.type == 'link':
                href = j.content
                for key in metadata:
                    href = href.replace(key,metadata[key])
                if href.startswith('https://'):
                    linktext = href[8:]
                elif href.startswith('http://'):
                    linktext = href[7:]
                else:
                    linktext = href
                linktext = linktext.replace('/','/<wbr />')
                o += '<a class="unclickable" href="{0}">{1}</a>'.format(href,linktext)
            else:
                for hu in htmlnames:
                    if j.type == hu:
                        o += '<{0}>{1}</{0}>'.format(htmlnames[hu],tavutasanat(j.content))
                        break
        if i.type == 'h2':
            n += 1
            output += "<h2 id='{1}'>Luku {2}.&nbsp;&nbsp;{0}</h2>\n\n".format(o,ident(o),n)
            m = 0
            toc.append([o,ident(o)])
        elif i.type == 'h3':
            m += 1
            output += "<h3 id='{1}'>{2}.{3}&nbsp;&nbsp;{0}</h3>\n\n".format(o,ident(o),n,m)
        elif i.type == 'h4':
            output += "<h4 id='{1}'>{0}</h4>\n\n".format(o,ident(o))
        elif i.type == 'comment':
            output += '<p class="comment">{}</p>\n\n'.format(o)
        elif i.type == 'div':
            output += '<div id="{}">\n\n'.format(o.replace('\u00ad',''))
        elif i.type == '/div':
            output += '</div>\n\n'

        else:
            for hu in htmlnames:
                if i.type == hu:
                    output += '<{0}>{1}</{0}>\n\n'.format(htmlnames[hu],o)
                    break
    toc2 = ''
    for item in toc:
        toc2 += "<li><a href='#{0}'>{1}</a></li>\n".format(item[1],item[0])
    toc2 = "<h2>Sisältö</h2>\n\n<ol>\n{}</ol>\n\n".format(toc2)

    return toc2 + output

def LaTeX(document):
    latex2 = {'h1':'\\part{{{}}}\n','h2':'\\chapter{{{}}}\n','h3':'\\section{{{}}}\n','h4':'\\subsection{{{}}}\n','quote':'\\begin{{quote}}\n{}\n\\end{{quote}}\n','paragraph':'{}\n\n','comment':'{{\small {}}}\n\n'}
    output = ''
    for i in document:
        o = ''
        for j in i.content:
            if j.type == 'text':
                o += j.content
            elif j.type == 'em':
                o += '\emph{'+j.content+'}'
            elif j.type == 'link':
                o += '\\url{{{}}}'.format(j.content)
        for hu in latex2:
            if i.type == hu:
                output += latex2[hu].format(o)
                break
    return output

def textlength(string):
    n = 0
    for c in string:
        if c not in [chr(200),chr(201),'\u00ad']:
            n += 1
    return n

def wrap(string,n,prefix=''):
    string = ' '.join(string.split())
    if string.startswith(' '):
        string = string[1:]
    wrapped = ''
    if textlength(string) <= n:
        wrapped = string.replace('\u00ad','') + '\n\n'
    else:
        i = 0
        j = 0
        for c in string:
            i += 1
            if c not in [chr(200),chr(201),'\u00ad']:
                j += 1
            if j == n:
                break
        for k in range(n):
            if string[i-k] == ' ':
                wrapped = string[:i-k].replace('\u00ad','')
                wrapped = wrapped.replace(' ','  ',n-len(wrapped))
                wrapped += '\n'+wrap(string[i-k:],n,prefix)
                break
            if string[i-1-k] == '\u00ad':
                wrapped = string[:i-k].replace('\u00ad','')
                wrapped = wrapped.replace(' ','  ',n-len(wrapped)-1)
                wrapped += '-\n'+wrap(string[i-k-1:],n,prefix)
                break
        #wrapped = wrapped.replace(' ','  ',n-len(wrapped))
    return wrapped

em_b = '\x1B[3m'
em_e = '\x1B[23m'

it = {'a':'α','b':'β','c':'_','d':'_','e':'ε','f':'_',
      'g':'_','h':'_','i':'ι','j':'_','k':'κ','l':'_',
      'm':'_','n':'_','o':'_','p':'_','q':'_','r':'_',
      's':'_','t':'τ','u':'_','v':'ν','w':'ω','x':'χ',
      'y':'_','z':'_','å':'_','ä':'_','ö':'_'}

def italics(text):
    output = ''
    for c in text:
        if c.isupper():
            output += c
        elif c in it:
            if it[c] == '_':
                output += c
            else:
                output += it[c]
        else:
            output += c
    return output

def plaintext(document,n=65):
    output = ''
    for i in document:
        o = ''
        for j in i.content:
            if j.type == 'text':
                o += tavutasanat(j.content)
            elif j.type == 'em':
                o += tavutasanat(italics(j.content))
        if i.type.startswith('h'):
            o = o.replace('\u00ad','')
        if i.type == 'h1':
            output += '# '+o+'\n\n'
        if i.type == 'h2':
            h2line = 'LUKU {0}. {1}'.format('n',o)
            output += 10*'\n' + h2line + '\n' + '\u203e'*len(h2line) + '\n\n'
        if i.type == 'h3':
            output += '### '+o+'\n\n'
        if i.type == 'h4':
            output += '#### '+o+'\n\n'
        if i.type == 'paragraph':
            output += wrap(o,n)
        if i.type == 'quote':
            output += wrap(o,50,'> ')
    return '\n'+ output

def build_html(document):
    html_book = extract('html','HTML-template')
    html_book = html_book.replace('StyleSheet',extract('css','stylesheet'))
    html_book = html_book.replace('JavaScript',extract('javascript','nightmode'))
    html_book = html_book.replace('BookContent',html(document))
    for entry in metadata:
        html_book = html_book.replace(entry,metadata[entry])
    with open('index.html','w') as f:
        f.write(html_book)
    print('   index.html on valmis!')

def build_latex(document):
    latex = LaTeX(document)
    latex = code['latex'].replace('BookContent',LaTeX(document))
    for entry in metadata:
        latex = latex.replace(entry,metadata[entry])
    with open('prosessi.tex','w') as f:
        f.write(latex)
    print('   prosessi.tex on valmis!')

def build_plaintext(document,n):
    txt = plaintext(document,n)
    for entry in metadata:
        txt = txt.replace(entry,metadata[entry])
    with open('prosessi.txt','w') as f:
        f.write(txt)
    print('   prosessi.txt on valmis!')

def build_pdf():
    try:
        for i in range(4):
            check_output(['pdflatex','prosessi.tex'])
        print('   prosessi.pdf on valmis!')
    except:
        print('   pdf-tiedoston kääntäminen epäonnistui')
```

### Metadata

```python
with open('README.md','r') as f:
    source = f.read()

code = {l:extract(l) for l in ['markdown','latex','html']}

n1 = source.find('!'+'MainTextBegins'+'!')
n2 = source.find('!'+'MainTextEnds'+'!')

text = source[n1+17:n2-1]
```
Tämä koodi eristää `README.md`-tiedostosta päätekstin `text`-stringiin ja koodia `code`-listaan.

Seuraavaksi kootaan metadataa Python-dictionaryyn. Osa metadatasta on upotettuna tämän markdown-tiedoston alkuun (syntaksi `<!--meta:Avain-->Sisältö<!--/-->`).

```python
def next_meta_item(string,n):
    n1 = string.find('<!'+'--meta:',n)
    if n1 != -1:
        n2 = string.find('-->',n1)
        n3 = string.find('<!--/-->',n2)
        return (string[n1+9:n2],string[n2+3:n3],n3)
    else:
        return ('','',-1)
n = 0
metadata = {}
while n >= 0:
    tupl = next_meta_item(source,n)
    n = tupl[2]
    if n != -1:
        metadata[tupl[0]]=tupl[1]
```
Osa taas kaivetaan Gitistä käyttäen git-komentoja ja `subprocess`-modulin `check_output`-funktiota.

```python
commands = {'CommitId':['git','rev-parse','HEAD'],
            'DateLong':['git','show','-s','--format=%ci','HEAD'],
            'VersionNumber':['git','rev-list','--count','HEAD'],
            'GitDiff':['git','diff','README.md'],
            'LastChange':['git','log','-1','--pretty=%B']}
for entry in commands:
    try:
        metadata[entry] = check_output(commands[entry]).decode('utf-8')[0:-1]
    except:
        metadata[entry] = '---'
```
Sitten vielä jotain random säätämistä metadatan kanssa.

```python
metadata['VersionNumDecimal'] = str(Decimal('0.001') * int(metadata['VersionNumber']))
WordCount =  len(''.join(char if char.isalnum() else ' ' for char in text).split())
metadata['WordCount'] = str(WordCount)
metadata['WordCRounded'] = str(round(WordCount,-2))
metadata['DateShort'] = metadata['DateLong'][0:10]
metadata['LastChange'] = metadata['LastChange'][:-1]
ReadingRate = 270#words per minute
ReadingTimeMinutes = WordCount/ReadingRate
metadata['ReadingTimeMinutes'] = str(int(round(ReadingTimeMinutes,-1)))
metadata['ReadingTimeHours'] = str(int(round(ReadingTimeMinutes/60)))
if metadata['GitDiff'] == '':
    metadata['ModifiedOrNot'] = ''
    metadata['DateOrModified'] = metadata['DateLong']
else:
    metadata['ModifiedOrNot'] = ' (MUOKATTU)'
    metadata['DateOrModified'] = 'MUOKATTU'
import textwrap
metadata['LastCWrapped'] = textwrap.fill('Viimeisin muutos: '+metadata['LastChange'],40)

# DateSuomi

kuut = ['tammi','helmi','maalis','huhti','touko','kesä','heinä','elo','syys','loka','marras','joulu']

d = metadata['DateLong']

metadata['DateSuomi'] = '{0}. {1}kuuta {2}'.format(int(d[8:10]),kuut[int(d[5:7])-1],int(d[0:4]))



```

### Kansikuva

Halusin, että kaikki lähdeinformaatio sisältyy tähän tiedostoon, mukaanlukien mahdollinen binääridata. Seuraavassa on pdf-kirjan kansikuva enkoodattuna [Z85](http://zeromq-rfc.wikidot.com/spec:32)-standardin mukaisesti.

```python
cover_encoded="""
Ibl@q4gj0X0000dnK#lE0005+0007%0S@A50d0V@Jq@}U1zp1{k(Z[&J%>>]1POJ5ay^@uo-Lftcia3@GA82G0001H>L$Q9fAqaru&Qf/M&Ea5AgBHpjobuY2#Bq{mCaPrsunzv%nO2HR$Zk(00cL4m))z<3W=E+000sAnlH0l01fzt01fzu^+PxI00007Bv/g}2JC<}4JLUK&p?o)08w[Ml{T^%*u&hiUhZ}ph4])061!-C[e&}N@&Zv[dXaA[OmI8V+&&.7=RdbU[5dTfmCT]=<r+[%wmywqQ?fF+Y-i4SA.N#=q+:TIY6KgK?)G){
@<X0B&CbHCRC#2N[oD33gFMD:KKQ83AN(^A=MmzlhU6pn.y)uw@4B$lVT5-n[.$qmRc]>sf}c-XMUu!co/tV$!@aFl%gR@@]8+OdCco+u1L#+*[bqQTn<vR$]fcqosYZ@WJce)7cy7W7lyII:BH?I.A2T@uo4a8>8!Cm]PGSqj>JV?%eGEyO{/tM!>2sR?8=KKwpTOxp49h>KX*3[<K6GlMaE@#*YxxUYaLI!g+?/?uufbIrOIA@wLfuudkY/a(<kh(c}d^bi)Tmpt.U8SJyGE&=*9Fpmtu=FkeSMK+R&DXxhp=DacszEku9DRQI3rRC
zaBfJFA7fMzlePDfkPx9Gzig7/[d*eH}WzGYA4G:Z?S%E:(S)W0lnsA:nayNcFy8GTu$p.jum5iqupEeN4hc4J1c.HQpb-YQ>2$:!D%iKi$Aloia-+@}T5YNdgDKjM0^G/r]XANn^FHgwH]*.-b4a51#ltAv-Zv?nFb>{S-yPUe3wP/?oSMq*.ceMqe7zp2!7T!MZJv+PEdey0)g@e<IqNO3FI[:7aiC[(7.qq{CR>mgWT-bsv04qc>u:E^pv/Np$Uw&6q$wb3lnJr<u%9:qpQ$<V=1{aTEoi7zy#5&1@.XX:ASjQb>kzgiij5U2HekU
*&G0cw-(aeJ}AL%JoTs%Q-9nwKYX[XlQG--khB>cU(#7.rb+^%2)E!j5hg?7&J#ZTdqa/XB=nxR-3C%1qEbHEaEE$]dpiwuLP3m^wN(D[VO&d$totDeHe!j#NS@39(j6Cg4(VI[FA4?x3ZIekd=[2T[[iW]-+j?NM%bG]NZ}.kaZ@v/n0Zr*a@>?tU=-}]UG*pGdq3Zr^pp4]cebRuSq#lj=vE@zAF^ZYx4/wRknVgb=%C+d}YNdn[W?K-vKaM5EI46fCVmhqa!?K[S9Cx1RwMO&6ApcS5h>h33Y4ASj#$InNK)-bD6Z9%Ue0e-A)05?
.Alrkto&2({FRD*Nm!L+IQz?#zu!1Pa)QF]9Nyye6!St:D.s?/QT7<-m^u)ilS45oKUc{yP.0{J(%[77HU^<tU{AP48XL>N4#O{u]-E<WgZ&auN*wrh3o#UO94&wPRGi$lm4VSmgybsP!XRI-t+}IBmO8bJ!erBE4NxMxQIQSLd7MpNxoa:IIdM$T4LSW?.NUnjwUVsw:B<%y9NC=MsEZRqJyVS#v}qD1I&OeP*}>((i&$&!D}yW=kZ(*.Lt!B<M&>Fv:jkjdvvPQZX{5X@n:r%zUY*6Okm#1j.gU3F!C]vNi^L7CIbG0Op{L+LOvmbD
igf^H}dq8N&TuL<^7/:1Uft*Wl4h&p(J<A9rSDfd3>}F04T9E.kxf[ZKVpO)<@PNded2BY7clbJc[l?c4bjfq98VZ&{AmU2*hOJ}3n&7Bt1R[1!6Zh$Kq{+e242-:^FbN:>$ZIX:K5BHn{TQ8l22UUq2&K?h]kBt[:]yObLA4I62W?t}zbX]NWXiuQ]-ka0kRY&v%F:<y}G/9*.Ds.*9z:Z&0G8AzG?J>Wk7GPA$>HJhqhpwqJpP(+4V5xY>-M-.LcA)C:DFtK5vvyT(P*XRWd[:>Bpx%np}1gtGIZAF@8)g%a]RdXjL/OqKx1yfY+d}
iT^w0@zp?QaXUL8b@B3DfDcnvpBE<NpO{}<B1cn{t.&W{<oZ<ij^^ixX//53R7>TM[K6W]&]Da9!FK$S6hf2+K@]df8D?}K)zI-IT3#+YTo[?Jt]G@CyhLHjcg59(.x2NKc>WxmmOD9k@Ye%$:W:YrSX?mj2uYayf%3:[NBeW3rvbpfOC]w}//^fwGA/T*:NApL)0n::16WFG-VhSHf[?[mcg}f=^CY2l?u(gyRMIiY6dk2XfxYl1CzRmQB86P*EWm2n>l.n^LCPj-iN307L#hgvVXA%![EmrGSkz}lZYt+{4nM?<3{qYlxJSV^x((69
/wMsH-jGOgguwQj8]%>@HhIGX=]5VqrfRnwS?Es6vy<{qy)>HSg){g1(Hx.B^YwLFG{zSEX#$]DW)*(-FAvRhTsv?Z.{B&+5nRaA*qoUv=7a3@yTs68HrJR1YfSl=.Cq}J/1Z3RD8%9uXb{38vh6.X^xn}9:@f-RbWeeKVLOTnO8T*o2{Hw=NJ!g&7/5:4zg5z&Gz{xUh^vv.ImMo@7!Y/<l(hQuH)waE5[FS0Uoz}J9(I/l[g&&45*m#$I[7V7w4)&/@1(e={Q?.+zQ)W(1N[B:kQWqj{cD){i9m](%3Q&JxzcnC8G}-5H13I=lv>-E
N:k@zlu:G1%3fPLJ$xKYPUQIEOU^o<C$GqCUV>*>mQkudycCNy>ec)gxM0kcrZKqu8UCFFz-+XJ*zr!2*dwJ9rmb*Y=s>Vu0R0&{Wnczj8i:MjOvNJD:-U!+C?UB}}:Qg<i&NkmgJaqLpx)qHnzUhI^0%?*V4NtlrUwaxc]gdk^qPMuQnqBTMI}][ve07-Ab/MK1ZScEC!:z}uA{w6=(t8)%k}F0Qz)]+a(rGcTr(PtF95OrU@!A:jpHWp/c(}rddkx+MK5la[1xG^?g>/{f?7@$q{mwXxpvPC8nDV3)(m(eg+ijTmM8)r.4zraG*UAZ
eQ7y!V{&n+V4+=2?oWoZi)*5!Ig2:5eZlBp*%41<XI+rXBZRr$XqPX6?cV!UrIL2Ff{@.s9ygJoxsaV6j%w8MWbf^77fRlq^oHEwa(v4S?:L7ryeT@sou(@{C-8)2xRXjsYTu=S)}oBO(6!]g0#@gQpel^34at57R*579zpNdo0<JRIq]uk*8A]AG2Whe)A9D@aSk4qOM]8xuwh@Ta[/mw2*cJ*8r#tXevpROTM$0t<){H6&[6<}LrM01!Kx+eLWIS#+ksoR8}}ZV/W26T1&Eetd)#{0H]/oV5-cR=R2ij61MQcQZv1Brqka$i):0[z9
6Z5Hn[325#j4+J-5bD>SBW+5{4uc:7G1THYhv}B..AmqlKSj>&4Ozu]n)=K+ueTee7(:Oiw{qauBDcpL5oGD?)S?24@nv7vy(4*RSX{RE}w!GA/SZSB5AkcA)rqIN=creptEvrQJPXKeEH&{1+>uaty3j{^8)m1AP-me0iNXN7:7=p3FiwgUT<C@Imfkbs*0v+Q7>bfJ*h5j/M:Uv1o0DzaLenP]HhGK^VDwCxM<deiB9uZ4]Is(PPft}ED*&>QmZ9J({KIr$GE#@YaKp/YGizEVf2H3[[{8$iE8{hcah63.]k$($9?:O(jb$y[BzZiy
)1NA$4F]@}]v/}A^<Fc49Np^?(lTR}H<8)K]9s^}y1=^!B-x!3JAD.ULE%x+F1%wOyFftwj>%$vkir6LU:L1KPSq)B9]JPUwu+$TBuH:)p*kfn:^oar[p&l2@USdebw%]qnkJ:bhLLEao6Iy&M#iAF(+V$xwEdY8y(1zwBaeQe8!8/:eD%:(5Zu/gOtCc21RcaH64dLh5pRAgsKlRNYR/B{A^c:H:(q5*ZX$l[}a$smGsFkcD:1abl:Ro5kkb($&J-+AfD1!FZb2<R=IX^u9YO?MhlVI{NhJ74VV$I:a?n5O>oogg)bQtsC$/TwNH+*n
/aCD$FHLBTV>RSHnTmtJxuET-?PQ04}tI@4LR9]/*LB#XU4:/aR{Sdx*RtY1wrFT(A(M&XszNhC/^$:ovG61$@jK=w1uNx9jazN=dwZ[cqFV1EkFZaw[{NvbR8^yEhS!Eg]ia/mb#5pO[f8o!k](b<K9AEOO<)q^ex}r>vt$Asr?R.nWoOBBagi9-[cRHsbB*BGNQJUF2zj3cQfzhLXH#.X{NvbYhgBa%>=0%4f6O4ke(Mp8<(W^S}1bhvX-4ApU::X2qEqXT}wR{2[^7>@bH7G&e7%ZW!4^&8ta!r5xY<+[t{T1EpL*9UpE(60@CBKf
e.X8{GC?XFWhoMnSLv?$-3#?$zURC4c?.MH5sp(lv=^VWx{0-r*^PuEf&zck)%3{xPM#vqdHPR0*aZv[-z7/$FZna@TJ>:So}I&q&n2APf2X<$--{sLMPR@%:so{7/8hEXScKCFHX=1x{xKgp-:SZU){H{iO5L#Kl:e5I{cumK1i&1:&WnkutCr]Tzkkf:W$y/SvAcF(H&cs0Rz(uCRGPh4Ecp@oJsT8<z?u7>!X=>4[4!55dN<rS(8r0Evw:/+p5VeZ171<VJxt!^>nPM?1zoBW]xm#ffo@OZ-clm7H-l7c!F2P$}Op<Ze:Rg%.0.ml
YN!Y55:j0b@Jr/G}!k{5@S1RXU)]ABJEesaLjo&QgCJV*8=8X&.4kfJ[[://caRf#w+TK$}$1aKH@1Ajp+{vV>W:u=)Nh09@CIcISz*Y8mjw$RN%29-8}eQ$gb3v::y45@z3HxVI)8xA/g*Y0B5</(o=v?7Yebf-]51xA=0mVi[VYu/NW9K5[d0>[Nh=BxYbP$oWK]:Rd4q)Sh%(i}M+e%B(q1GxsG6C5%gx-2HyD]oHsZRtYFsN0q(Mg.HPzoL+h:)KCE(<oVgK2C]w94oHV8Y=Vw2=c>3bRGhDLZ7lFn@n>GO>^9dzfW]yO-%5MnrY
?[ONtC}O}LhgYWft7/LFA{kl4N)23vcO}YAK78a-*2Uu.D&{%V&P3SY2xXkg4TmcYzt7Of}%!EG4B3nn[j/=K?TkO3]e}y9g+kJ-<./:KPeR0@P0v=9rNMHq7cZc-iOrdV.CO/&!1KBdO#Qo!CKP=*jfUEtQQQiln2Y)sDDqygz%&F21o1?>=fYSv&s%*VTCtSB<ymdhJ$=O(f6Oz2pDL2K=psb%hXoS9gUpf^@l&3ZlVkS>cd7$w[)eS&vJFmR=#i7e[?qfuUC72tz^:3WEquC$-CH&lmb8{Xx*.GVcum5tGT.IdNygd2{b!Y<fYOH]
eH%CcUd6t9Xw:}ye<L:luXEnT{j(:GjYRP&/wDpaFy*CqE1Nh<*1GcIm5dIeW>J0B]2L#wDLYgEV-IG24lw<k[rN?5Xy/@hwyAiB.<pu<q7h?tWE)7eu(7PoCo3R)t1.vn@@xNVw8D3S:}+6*O%=:(1@B=Ct[RR:WE-jtV!kLfWMmyZotN!]S=}b>KnU}Y@8!-h(@:$n16&6dBj%3!h*AklB(h/h/Ja5o2I:lE-[KnqHE%)6K7oYcz9DF=[1a4Vcd8Q&*V4ac+WU9o]?g$dbZ<AHbbqR{*hwy(Uvt-@J}yyq*U4E$u/uji4slS5wW.z$
8<}RuV+UgKcrSSeLiQCPU?LQ)zn36LCHG@D3++-{z>}PUP.[z/7qemvKf77Rv9yN$@bi9dLu<xYT>:R!w7eCU&YUXjs:Jf&1mh+Rqj<}Y*caodM72]PUOCBCIAlGFyz.-rRaJ@1t:*!eUI0ZJy%zo.y^3G7!{+0mvH[(D1?3J^5948hgX30B3**J=mbp[FzrhWfdIYV5NGjIVwY)Vig]<RM?Xi04J.%I2g&qRq66jFw?v]f:M-ouT*SClhX3#(RJLc&3a>nfFg4s]{m^?uY=fCk&QB$!/7t<F>S8fX<L64qEQ@!8i]+R/XRCTn!nWX=@
rG*0}:k6>aUX9D}q6%1rzHi1kv6Z9g}QQSKPuc-%e8A98W!]DprS21=8Y+Ek@L0RO@9t?(E:qR?2ww47GahUmE8mL9oP1nwX6nS@}#bgX.4QGiVn/F3tR1pjL04!Wo.M0=FS1ba&tm5bZ3>r)R8+rM?&kgBt%JK9A#?$A=LrTvDrlkL/5bC#EKIP}9N?VIVzkqyz!PkTHP(ZnO}eB6KfxP2F>q?OGEpda*Syjo.ekl&?jon#)SM=1rLl9^Vv#SQmD&.0{Jx3{mN}ouT(PFrfdy[rKEo%-RK:?GR6$C7ysD]VGG(U%M!V7SyFH(jMa32!
EXmE6?[C&YDplr.[3WYR4l&Uqi)BL@>M$J0{1%^IoFg-zhgcI&!<HX0sC}px9]Xi++ZOQs!v1WLQsLjlL3Rx<HFBE6=1b(3eW7Wci:%<O/?%<4m)0^gGJ)qk?b8cQ8WxCs)I8*CJO%[UEf1+E^SZ$yup&dMnnF9ptGj7ePkiXG-/S%@B%vOgOgR7q0@?QpVJWkg+-u1zF.4p}Q6==QgWboNI)k-MxxI3>&?LNRH5P[gTE+zHMOaG0:DQD*iSX#$rT{D+Gv?C><m[Yzue?wE:U:(uAn21$/WHkJWk1T>Q$$#bMNO^J(XIaVw>:AYIYonV
.+MD*J[m^[o[>7Gsy(TUxld=8+k&9qJ*&ZHJAmOJ*KQDA5dns+eQ.HYR:8JywcZdi6@U/>6$Q6G=q%a%Z0nx7Q4Zqy.entI5:O7IwNaB4kp-#<R?@$&j#}+LA:/cfq)]j%-WYkyg*Ohg>4X{f&?M%qs2<3qnsV^0YjjPB0>jPR+->q(c[bYp??<n0o^2i^WNs=n1.XNN9Ob$IEA(n0ojUy6(uenk^AFD&E@UlTywpBXpBAl*iYZ/iW+#kW8otr:gZsYTPGhSEd?jgJVx8NRw6E2=zOB!4[ElC/4b>Mn<EzF$<1zN2J2r)fGUVxLL=FV6
coYxZy?a@P=hs!^y:dZtvp3bijHQznCN-yD8<r<4XPg1P@>8^[d-[Vo8$@p#VMe#bY-WHa=wyc9Z)c<zbrkf=OkIk#qeRw4ffT2LKqVNh/F[%+7yl&yXvBg36MHC=:1uE+9V01tO@y{:^Q+!5q]V}36gXaVx7rgGL]c=>w0)WAhmQZ4uV6OXNnYdnr3Qjvk$+M-N5f&u7i7^jfS$e?+U8=K[m%V7=U4-@7/NL/z7oVpAM&iR]UbyVSR8maOZTB/=q}9>.6o.4bS/VAWr4@2S.]RVYACEOTeTC4<hrh9q/@LIpE3%)>OD7x=Hj+JgJJ9e
:!$#>I8tlui=17vo).M}lKd#@B5a6y0X&rHvxT.R6/?ZnqN>.&^INdw!Ky##y+dyA<?XxJP64e.[GsBhY5W)thJwaAPy{rB/SI-]+dhKRGw})Ybh&!@puX#+kg9&)Cek*?>rw9B=s@sa7hv=9aVsj0GGQg)e?k4D8lKPSX:Sxv!)c&.1Ai^/h0q&:)rbS@v#GCRlk41-!V/Q&{HY^]y7NDW*D6n22/>t9S*Dr7dC&Jk=4@7}cMZXMbY^2QWS%BIl9PUti-cA6[zniF-B:+(v$^7wWHvwYhlU/JlB2w%*9cA:{A4D]tQBJ3<@d/%wzD>d
?}C5{s&}:+5a<UtO8O?q*k-:8Dw[r[mvmQf4?o%rf[ho!SMCF%@dSlE}ZfIAC5J>W)9r8#cDu?n:esvvZ](KtNirk/j<1ESWiK7CL!qsiiFU==50XCXH.<T61QwA7tddBmo.Hs{>lD=#@FS-%>3=ro-%]UtFZJg^GVTXnb)3:gIhWQGgsSbShI6-DZo.gh)[%4(bLN:=/EBOqHfK#dbC]B>D8Ua]alG.o2G+^Yi<:+mf08N^p3(9FOf3uX?@BcQ0$kO@8NnBFYL9{tLn[Ul[8SKT^#DMifjfzSICl0QJ5M&M2=07h=Brysc5uUXXc}M7
mu9*X2EQa-QDpFkLKi6je5jM$ALc50[i+)sNa!M0!7DzA{k>c<Cch7eCAdG^5s@ZA+l{MAuPCe4r.2(f/jn(Zh%C$/i#*2Fn8apGrit:!R#*$bzmt}EC&H.-Vs7ZQ:<R.tugRbOz>9/US0ppk3V]HQPT^$^:rp4n=b:znZ/}PWon]r[!LvzDW#o<qV/{[JtU[}lmU-KoW4Tlp9D5rONx?NPBuQDrEaH0R>h]^0*JBAJwEyEb>mLigObjC}QZFM#cH{/J(6-6VaL53A}A$d6hDRikleB]fk*WRy(nFl.I-h!qOdufyjpi(yYh%7tMLEae
0Sf/Ok<YRl)Lg6.bhmN.TL*DA?<gJLT3llCiXIc^SANQ9iC1Iys(B?JQ+eE)cL&YLflvDKvj&9eg:J3NO.y4098{<tz}j%3wTQ%K=-wTpeFVo!<}&UFgCNF9A@5E*/#wCY*}u%lnQ@>c<E?vjr8g67wBJ/:w@b$?BmYO7&23Ig3{$N:d$/y3Lp?ljpmh?${O?@OgXqV5B!Iri0]mc0I8hlH[Yo0b:#*GcKd1M}x$FmbDTqtY:+.E&nTsqKM/c-!2knm5Q?8K}vReL4M{9FEZsVzFe1y3/aZl}Niot3^:XAq34-$3QVLb#mdJ480K&[n>
]uiZfbGHp=1iIGPzKhw(B#y2v5XBkuG={)eiT>bt%hUeStzkW/Gltr!x.5#EFhJFHYOLhZ)ZIv4*hsB)g+R7zt%i36hjK^52ACQgs1@[B*6wsw^>R:q?JNijak2*jYc=)4kX=FTBdxjcV3zH.BgTY4r@IXx%5WyjcNIud8Uu)TB@*DFKB9HY)E9xJOPO8%av%L{-9>AbNzM/mASbVY+LNel<&A%u4B.78*INauj)7Bcq]V.TaixoO[!+PJ*D)N6&=)/1u?0>X8:]sgzcAR{btSmZ/#C}M&@15i^DG=9+//?u*n4D?mD[HKLPE!0DLcS)
Xk<fwZcPKR-k6U/WY?fD]!(T%6FAZ<TYV2n+T}o}29>d?0)=IJt0zF}ywU&:]bhD#XOwxTUhWJ)-Ep[zK!.nGGNwn2Ms?<vtc3bV7p6xi.]T@gr5/Q)h[%!BnwF!*Ymz?A)6(t3?v@2!ihmPJzqdV1m#0.vzAGR>8Zp&OWKnUMy2}s3?c6$FgDCyfJfPWX(KHY&<9oi&+z$=)=j6>/^&6iY15c?vRfOJ})4E&a>so?Kw$d[ju-[!2yN/2&OAqmZnPq9xySQHs6wwL%0Ew1ul^eeRq/aC#YWn!4[X$%*Q10L(hA3lxe]:zMb8@P>dZUx6
V{)8xVe/f}[ZK-xGWg*@OMK!?Ds[U#7cgCRE5[!=Q.%+G?Ni*G<<9Y0bXpxanM>Mt9a$bqj63qOx6TsHpT[xzQnLSxHWgr<1R]k}Op3BOD7h+gY4y@9L>lvqMzk%)7?Ag>fpf}brU=4rYwjaL9^1L7SrM647ov6cWx33!l}gli+f25kN+K0L]qh@fi#-[YxJocC*M)$hYIBiDdvgtCQ3?YCTVBqE]8Y?kX($3o+<WPHER=FIyVUf{Ix8pfV5ZZ#9u$Pe-2lf2f6*>c/mq6(Oy2iuRw3:K?&0#yp>{zb5TeLr?TwP?1/2>zVW3$T/Npbb
F^Ma%y#+Bv(/Qa}N4UFi!Tmr}9VouUw:+@Ky3{!]S}fUe?i2!#Rii>0xvCl^U=@Bb+<TVk3>lz[0R/3XFQ#&ZX[(9Gx:4tc}?/*V]}Hufuo{]CDWrye=2G:MmY/!9(KVj8yPUic)r=)HrwrC:yY!bpsl(HqsM#F@f/Eeu>5y7BT/?KUZ^k9-uL>GHmgUO{j8nUR{yohY8m=@0W/1b88aRCWh(6lCIfQuyMI:[VJ&l$7B)3Sx@IzohZ^8X4KtDW7LD5JSZx{iT:LOy)D(h89dpZZW+@ZS*UmF4D:]hUr}u5WADTb2[gC4#5w8u%vq?q+#
.HWyvYOH&-yM+eU)JI8/7+V@Mv?T0?4PqokbxJ!fYm(IgYlV{q-5/b*zDy+ru:ddRCs!d1>11sts/MPi7v9K??p>Pkw$<%1[9q5r0[:>U[39]obC@SFs)wn?:DCJq^P?CMX^^=?[-C}ivJQeT1gbhPt&Lh5aF#M@{kDO$@zcg$z]M88Wq9asvnM9d(V}d[Um<]J(+p-8j8P&KXO:2SAor7b+?Dasj(jZyepgEHzf{&z0>hUrjGv.idVvG(m>axv{u?O:c+ByiLIE8]j3Iozf>tw[.eCJ#9WAQS6s#/x?%lV#q/H%KkM*A=)stAp>NeG1
CpXc7wVW-^P%t+G-tr9=Qd$G4&1ZD[/OyO^u?itfCy7gr1thW]Q05dax%t)mAe>:yVB/{q5#olU<?U+/ugtOM[%lSV96s5L}a!ey8r2oeu7C]jmMP2J=Z@4+a1D1aTCdHlN}&bUyGw9Y@-lG%)aeo+}<DGf}n3tJzu[{-K%8p-JCmmpy]q5LNxkX?I8$8XUagIClHptDO4Fu9scAYWL+Qn:Tl83ZE{P&BxW/f+sH&e2oQN}h3%^8n=6oYU]Rg-UZo)pELQ8ftK%9.$MaF+CxwEQ%J)zijB#BOv]SSto?fa{?MEY-3/:a1XdyOk}ZQ7uj
uAyG$maur[jLyk>/37gwXh-)-){YlJ0PXmd2VG<f+-P?p4<wSA8&y#1qI.MmmgA^qh%7Q05buViOPqHDg7I-xZq8/RNfj1j?z74.Oj&CQ3f/BRJbo(J:n4RbrT.FKx}.AWV}hXYe]k4?o2T?TKEv-&Xwl&*NfnLhZYYBV9xixQd&cETmOE)Utb!QZh{gW8^@Ugx*Z!T#v0(N:M%@%*^mecD/9g21n4Zm*2OdEyMDQupO97FGEbof6IdFi.6[hss0w!2{/%%i7!Ktn3MwK8vR[3=r&z9F}]QaPF8n>iPUQm@3z.zE4Jlg:PgBPZK=kIaQ
&7ARo-#ryZew^?2RM>xvFq5PH[BdlrK{:!J.uI>o>FAU5oRP8jS5pAI33W:(z%T:^oX#cVgQ$=Bg&8wJaI>?J?7b*x/Q!Dtt?x.kK6#mb4AxCD=90Iegj$DYTQk!ROjW:1l0&e&BW5RXGmaCKeZGoyeh$XMVt.m$ab(xffk?Lg*)37Jv28*BazJmWAVkg!+#xoCw5sAVAZvU.^HBqACv@XPkz3%(C21H+T{y{BA:X4[igS2?mp@yoTY6IbyH$Im2oE[!Fl#Zvslly9-6k@@-]9xJC%=U=h.-X!F?qQxJGcaEXIy1sKcy=[&*)d0YlI?i
MRtT3.h1?-nMCM4e?ekiO:/GE&3}MLM!v0Q1/H?[J)(L-Yt=VUegj{1YRWNMYkBDJRaN8*PBlP7HvFd*WInmMRfJUe^&2uzNbxdxw!4dtShP{^nk-b>nMl2GM$jp@Lf.9g^R:9US7QtHfVsAy.8#!WcsdAVNy/f@WVT8^o:wfE]@SjSH1gpG2M^*yM)jNXOd*6ZAo]mgkmC}:R9?rAJ]o?AMRA)HNN6OA9S4D]G)!(HQp^@IJwL?scM<8zrQ{{?1:i2Vj>gvOhU5iZ^SSzgcdp2qgv2Ve{93Nb>1N9@N^y3t^(3rQ+J=DRHiaRN9RnxK
>([t$JNHM!Akur&fFES0dU6)*wm:9T+4QM<Zq2iU(jgaZVk46..krSjVqJ^+&eVL2{k3}mx8yR)BlW$NU#)VH9Acw[QGZMZ1kuLpcIdt/PB&s$BT0UMKpK])eZyw(Tyyoq=nT/Mat%7[kGjL2(>5l(vf6b^ehURaKa3C5&iiI3aaf9zBX*=O&9yIl/35S2+b^Q$s}Uv>[pxuA}6DS>p^OSqy*&hJXnk+UhCcH9OCY^9*tnw^H@4@P+{V:cAU3FZdtrgV}&brOI!Z{+Mhy*xfP!vesAdNPN2NFG7DrZ/OO5NC2NJzbd{Ns46&1AjWwoc(
H/8bfXIRAk4JDF0oQ]H!u^VKbz}GOHf2&Yn.V73EPTXwI6k#Yv.IoyP:6gun^8DyDlN:X)7-W166V8(-JDTj3h3*q8?3<!PAmwi.ANWsz.m[X>:WcetWW>qdi.4+TR3yElw>v-am>R]y&5GZsji)S+NaA:.FhF)U7?Clu4L<rs2$$t}v3bM.j(l8vAe!)pH&V$(0!q-5LiY0QcNs+Y/rMY0?Ino3d1@X1Kheo%3@Tp?9>n.ZsI6t{UT&Nz++(@jt{q*9@<#.VeG0U2W*1Qsew*W]HY<x(XF&X.H72.b/G4yj?6PUn5(cSvg#}rG:?Pv-
*0?Gvo4F<)p%PXbj1>x&>zGkaDnu![/om!c@#Ci2/KjQj!v@y3h=SB[1%BCpn6^Y(ro&tYJ!Z]-.MNT#14UtUUPt0EwY+{&vM=/u<vXG24@Q=3jZCCw6S1w*JJE1cTY2*/^pu+>jBJLMpRA&::-bJ<:y(E&hyB!ju}<W$b{Z/OU=5dpbnB9s*04lzx!O<978UbGl)U.n+Sz^ONWp1WwFiC}n0Zu:juPpiMDS(C1rKO6}TdpmUPCjv:)Z)zNawlC.rCZ>l13a>t&<L{81*&OsO=#V3>cGTqIYeUpyg!*lPv^nsCcy!92nU31#xh<dYi4k
soj%%qFAUVjm{98x3/2pGvHa.KL(iH^et!*C#xgtZnrSdOGPwHs)7CNec^]SAhgL9qXVEm:Z8-yW&N2VB+qViOv5zjQp(]pUe}ZYxi^hIHbLkLl..9>?K}Y!zPJG0h!qy(CJ+U%jCTV8CYvnk{fxH-rFXX!!WGAglrQBh[8KD@I3&-gHQsk{=DT)5KY*OA-Pj44R=!VdJIwL^Qay@r[2cZ#pWrIOV9BLE*(eV2J6XZ-8=#2MLY#O.*%+^4y%Q3KHM(!OC<a/{P}wUi&:IjznKC1W=ND^UIH+dBZ4Ko}&rIS]sa2D8&Q*/v6rPBUy?d?8
3nTfJ@F4HrTx2P-wISR092!2+x^[5tH<MunXA[t3)zK9z-D8w=kZN>jgrBlYj--aBXnsm#4e[WU}IEMTq)+gS25$[*XBPPJY>9:P3zJj*O$MUj-*3UOdibml?7R&eXz.lH^td@z1!}AP{v8GsNc5gLY5u$v3Z(Sm{F/:yQh2Z+tZX1!kyyybtb^{:=Rm[3-i5{BN[f%MtEezw>.5Ns]LpH#Zh3M=y2qZ1?$Jwsnea*(4JJKRbjhL<?HTqrmS9s0u$p869*kwWjlSemtE^D?*xQP4{x8M#laY{V}*CMZhEZ>2ebAqG2m6r1(P}jmJfz{{
f-4By/>KUYB<2+wQ%m<X->9l$x(mqp:?zWGDeJwn?GE#+Ql&66OVwq&}QOIKuEfU[@6jIjs+%J@rNJh>AkpEP=!E/%LJ05Xe6PgZ@bNw.XIyICjRcxV^]&3ctx]g&:H?e9HIj3Y9fX6}jBkj3Vw<m51X%6{ZiJ!{xYM%MjloPxlSQ.rpI>ne6?NZ=7E3TYh^k8ipEWtu{-R9&9J<j$+r1?bL9<*]<bRQaXR2aod4b?wPFb^A{vKtZU=)kOTSXy2+Xt9VdEJO*oc&H%evO{L{L@XC>Pt[yCCBL=WJnzMyQ9*>[ihv1H!*dbjGTw6l^i#8
8HMq#&F4olGbfkSHC.7JRw4?bDt$!.2YUuZ*Xc)L{$(gFky&tc[XBxth?ZMm-&UKzRYK(7HmFI&UAWrCTK6c4*mXR<wKv8]Ujx[C5c5v)hxvevSpo7/X6I[+ZSHEHfj3%IMfhXsW.2M7O9xPQf6[[5]Y@Zfv(=d}:rVTW!A@Bz?l@3WuBEH%+Aa3f>vr348Ri)AKwYfMP%4>Mbv$U&q?nRbCqqC&s9J2{n-2.w*<]}+YXh=QMI(h[lSNK4p[..E?skA.oX{Y].tV25g[Oc/kY$:?=-E#t*I)2hs7/:>xN($QrP6n-ti}:FS^Qt*kuCpC
8oZIsAmc?BV-kYC}<ibRL$$]xJk991sR21UU7nU#}yP^}x4mS*O0Q]d<t@:)v-90Ce+1UU{$b+xZNAjTS0=1gJp$/m)Y^yHf)QPJUA20]YpZw7a&)GR8ocag(^Kt4EPJUzv>M.vMnk1cuHs5B^cT<2C)-Y#MNQgumL)5dCJgx&vMiL<DVu+)D2Z[tYiF51m4y>5[kujN-/zkr][l%YsI#a]Y7li+k3WsoE!AeB8.LWA+L5>uUxF]2(XDMqH@2QWw2Jexg{nh?%fy!qPnyQFhmB^0s7si7TOafjS&24lg+9n85dXC5(AHI@*xnB}UK7(z
DF4(hl7A@$5-!FSzA]X%({V[7q7(R5V<FXIx0XV0<>0{I=LMdSkof.J7G-37Gg^<Rn8fbuV&6t+CJxh@Pd!MX^Ws/():]k5O-}BA]9Nq>.D>92N%[aOchd#4SL^S!sMrSMO2<iV]xr7oNr#.xz6YjLEk2NGvW*DQF.U0B-JXkp*7I*(]7fVg4?J%)t=G&*y]hl%/G3?gO#a!(ew[I&](tN04}ri9z+kC?@{3qNsSuTzkda$#[pCMpqKs=.2gj.YBy#U.)=YCTYR.N.4sowL]IZ2/^Wp3X<%UzfS@1yy]cn1EE:JkMyXwTr+z&IEgWh-p
.Fq>3:Qw=5l30ZhWZqo-eyIL[e(9+veeZ%-!LAV^*N8h+Zuq)K?=mtJOKY=.-uQ.7u:5l6j73?DxxdF$JLH>g:SdxyeU3B>l]Rr[-v7zN(a9]kTYU-?*xADxy9UEVYX8.TOS!AiQQ<F{X/SM!pWxJWYt5ROU){RIRn<uy6$Qn/<H)EnBDey4ymb9z@O&T[D!RlUQYbY^tEsefr.Qyc}HR9?mCpkUK)qIk^xo-?8}%6b@Hzo6Cpmf6:jCaqQ0MWR@GH*UTUYMRZF?Ve3Ox^?M]e2UCAN}+mn5QbSObf1}yyhwjD&Ge>OxV{tw2T>h}B((
{k0/=ug2btg%E)>zh>!/NZeZ24c-fo>ZzTB.^emI8D0]am51}?Wt=t4[@hwZ*/>tGPaG^MTNg6k}gjClRC]q*p<adFCz]Br?1ci:oV>goMPs02kgp/#*Se9R]f>uq?TL*OXLE}[A*-n-P1cZt/RETt(J2VdY>?=-L*Mh:)rev^9i{{@8(@srz8[l^{-RZ/K*%wb@Wg0}ZjX7#>/2n^}w+Xg2kApbl>$+wnjRP6@f3j3NRj&eycu1:]IT%)sBnVQ!8=GV-y<JwSgybJ4<-l%eZ9Kxz%=1tP3FGQr6$F!CeXk9<*V?n.!HS9*(X+/8-pk6
:0Cm/X$dvG1N(cfce[[&D-b+Aru%$w6V&JRJhO5>[!gyfpSWtBH0@2=Hp]XOO^#HOb2}k$WJKu?=zEctj(*5aK.<nuOtF}?}H2QEHP1eML$u$49Rh#wDg>dke$9h*63iL9RZ>FnU#Mcle6g^Lm8Kb^IYR23%ho2E>Q#yisu!.whxQbTO]YI[!I?@cM*h/w>3%:C81-Eh4S::m7=#myiyeJr[/d>=Fu/nZ/Mtqd8POnN?Jkw<[LhYggZ(/]X0N^(Jl5k)fb605lMngEiltnU@oPP)MN6c7DMb})kzT.i)A%haXg@NGyFk<(zaKyDF=kB&
pJJFh?[=+mn:5fYk[34v<>mVloc{Mc]/O#wYlXXzC}u5DNC<Js..fWF2d%hldruag=N]j@u:Zd2[EGixXX2sfH}**dTDq+2/f63):#c3wB$QtGnmUHM*1BYq(Tw^*85d}/i@<.qBg?ER7G.PqDY!Ug2/pst-Un[8vWSzzr$-#}AQ-u}@x-/]+W4abK>g/3XKS>6U?E$1LlaauL+kN{}.^@DTKF{&3mIyv:gWM0M^&UIW=UXXtpOPo-)yO]RCM4$8Ye&4^0P<%35nn%U{QV}!E=9ao1rvpe>:?yw^rVO*Iv8A!Us/Mea.$/M@iTukmZPS
dxLJ8OMri7!$DD1cX4b+v*?:X}!%55&[mh*E)+vmQN*bTNSb<a/[-+8*{::-e#9U*fG5Cn/FL5Dwv<4#am$1hv4r8*/.:Z^6aQM8rI9k$MNc[D:r.0wnC1%E@:M5@:MC/[mOXZ5EUl1x45K#r6h6y(2B]]yVlp+FIqjW%=kRL0^wdt&{Rn<3wq^18Wu9{fA-}>&9*%j79diyB/aPAl/gHL<zC-bpbm%#PZn(vC4PvKv((YHF:r^#LTMA2PCBYVbn(q6tu-z9M3z)@ugFTdwYboJ-2:7]f2bZmZhoFj[Xj@t)c[!UeFL4G1M#uC[/(1?$
ks:HaTYX.v8sM0]880<vL2:7>{rE=U:nJB(MFsX#e9g+E:PoSZCHhJd=-/c.DFR[m3{04SdPf2VLvjMl*:1FyXhU{<?F?4^inVr)7$[e5=E^lJKat&TNY8ATz=EC&{!R2E(v{[o}iesWd[aOM2a8N%w{l:!QwqX1w!EUYY**bBI45g*.v0Mo.SO.#eA{mS7ai[pqrMLG6>z-JsNtHF.dE55e9N+F(Kk2Mv%6nr8yWvYj0Qd1cuGBAd4S<mj+[sk2%9l@1!}yCaZlRb9)Y4K>%!CP8f8#9(L9bTI<2s+*I91#Qi:X)Y3!V66LSk#^I^6m
qT%?8M9N(XK9B:H*6hyLTS7<u[EDO)N}:jF%adUP+tl<ZyykVO9>Cr+T{IWKh3V9BlgqIQ6v2Fz*cg<%*TH$>CXF[qQx6V46T-ocEw[9n95A:P2o3c#A)HRUz[0<]eDV8OAj-Lb}2AHNC#&]+K-oQ{7L@>h=maO4q]v4AY)d@k6A{xDgK:@o-y!UfJu{e5P1Gzdw.}-ycZ?MYwIQ1NLDoh*svl$o2nzunJ}Y0^A1/e&vI5x!LQ1iFW4#0+E04m+vP1f-&ce&R{Nu<zx)1rD(i}Ved&W>jBze9nIKe^UR>0D6!F}o7UqrWO3m9md8Dr{8
&$hW?/OxftZo9^#51hmskDu{#op.R=m/#n5p<0HwunfOxeJzJ?eee(aVjL7G)&4Rk.O<e#D!gBE-(XHK9Ko]>coD+eu1[6EjiVSstx)#uMb(dxaH}[^Z{Z5GDqHqR{GztTmSNpN4^:lnb.X**><Vb?gFb9T>.B31=GTWo53A^GCb>UsRA4phebV/#wpcgQI7YP7*?ij$wdFZ=6w:Z-k$#xvB7n2nxe>JQA7WKzG/Uhw/[]=UAiHEY5Rc7s8aBVXrhI+#+Z!X:2x>([[1m-rkVe16dLVxl/j-n[WW=@JbK%924cYYqC#j{bjE5+L9l$>z
ib$bL%70l<lnwfVt8@E?XH6a:1h/Ib]tvZSrlr<lhP-*VZWR3+V<GXW=}.3gAJ(rlgfAkH<zKmN0FI}J&=yLwbRn?$:zhEzw{i{lEi8Tp!WgL=(NdWRXXR)IFPk^F7@T#Z^>8qq{kC&Xt5kr-5oV[7>UgP(Qs8QwfGA^2+}*kb2@VA3mXc^*.?{H<Jy?L/FLTO0X>l?3:zjYc===U>:NF&(]Zzl026:!kjs{cp-BVfgl/PF6o3J1oig!0cHltUIIg)(TM/]tbxGu[g]yA@=nDjAS8W*+YUVnR./U%Yn}>yw=1tx9e3$W1+U(14KA/3{x
i14#2v}gBd4#&F*9eiYoZ#yTI&Wq<4SKpiR.w(wID>gEOu=73>StsJWJwd%59O-iF-+L2MMkKwStsuV6AVmgcN)Ark/=*:7>4mW=V1W-#b<g/WO.c2^t#TcjF7qJH/GApiBHfvf}#h?FvXm$-]!}=0clO.aJbw?WuTX{2BPo.cbB+rJe-OK!QM3?Iikl!yF-EH#OOQ9Kf#]XBpO^8I{n.V9J-h3MtHzdZ97#K=4L6AlDE(Fi1rh0jPhRRxhBQ#fhuV$g?z8R*hBAcLo^dVdlv:CdR[njR+48s:hP#9fB-y^=j>KXYU:e%BVoq./U@TQ:
T]4CqZM%D(@W{5*GH9]TC{{6>0=abKA[mTiu(?[v[+ET=0@Z9Z9v>eGy0J4rel/+E6}AW5eW?Kd<yjW*DMKlhsBRqpqzdU?a+L%MR}LieuU[?Xr-nGsD->p7P@tFnZ^=}P-QjU40>US#]-$P9fEFM98)&Bs^p)Q1k8mzRKfDlH!OE=ZV$-Z#rZ0DPtv#S2/EEJogt7IaXVbUr7g<II}y=c/mjL4{g*!()Kqh#JtG5-<m!#=wnMIVD}])h@CdQ{or^i&dh-/-g3^x>#10nfnUE(&8Nn7]ja<in5!N$Y#B@+tJ?yFK84L&]erm>>e}Bw(W
.Oa@>!j7vVdgQPDdSDf]8st[4hYh=/VYezaSqC^nc#cD2]Eu:$U%PLRPA?J.ric<$y[}RhD<ul!f)ux]C7^MMhn>>gLs22SYs*=b{}yY9.!0f}s6.*4^D@xbSBeYmT#S>Fc-1#gjgvU[FdioWzvA5ocd*%-N)W#{).7k&H1+DoOd70/hw8:{<PK/iMD)GDHZzP1>of%#k8hZuOTcR!c}GVoH>aD!z{%o&:q{5a?e:WB+>g1)cri9W!1bJ8a+Ufi?Q(:hR}qWMqM*MCZT2DpQRyT-}]D8:&9s]/(-<-mMB-zhHFgnuk1]TkFpUGbA>-LA
WLU(yR2<v7=YRt(7gcd=ux3M0qIu<MDug:=9/wko23t[5YlRW-e12RaSCE2LBn0NBgH[BlIq2vx+$}?IYhMHZ&K>>k>?gMjIb9[HN9W8RsB+Hh^uEGt@M-ffo--HJ0*apEl1g<3mKvGHQXO(i(-45357FQaZ*97I.$z2c+hF2yf&K^}.bcjr?wMrDLx:AQnF5ONgSGlZD47d14lyF%C[[czxdY}7erI9Zpl:nwRbkQDB7-K3m05e6dKBu5gXGPbMo!fhqSa.$!O?-!}K$icwUZzZ8q.]HS8M86q!ti7.aO@8[N==<-c%9c(V7t&mZn+y
)y!K>uWq{12D%3r.nh4B8V6A98[Ep-zcC[7<6sbN+s$c7-OrO6@!l5Xn:U9CE4vc4lI+laG%LqIA!g=4@vSUrBD2oB^[@oFW)>R]f3+KuDY#7{aC>ohpq5842qx#/OOwQSV?8a$FPQZJ&SpqSiE%fANv={>3gB.pa0N4y7(#cS^/4d7)@z:[Ce+Y[y{GS(P[NkZIj4}3{o6]Y@1:.{@l8yUGBF}$![d1CbM:11e#W>uw}/#:voho-/i1#q<JlHy)LfJc^O42]ZoHS6WIm0%^fKUIDZ9ZxeIFL#J4RP1J)/SiME-PN<:5U/MlH%YL5$v6
EicS1^Hr2*tW85)1gA#8^@f67m4Xgj^[[Nm8HgO<(^}-vEwdr^9^FS#sa-[G7]TuDeo+aXc.]c{C&L)$Hc6geaYuSXx!BapnXVuM0tOKU[oI$8KD7u]!}kbM</LaC&fulq2Jb*rd[-]Bh%=ll7:.(6rwVv#Cg*i:b$HntRh(dS)QXpkb=(WBx9ypUxJ/&3DYYT6TrZ%ggli@?1hK4J?cd0OpHJ)]UqC=qM3x-k2z.PoXZWf8.@^t5XWk1#YmHHfwTnA(gig7dbU.+g/dfuYkZtL{lMgi.:a3?idB>nWkn:JgE1jJrPZ%R?f#**(f.kiZ
(u$Z&ZqwsMv1m-+kIQkE9g-hxvi}CRvCeK2%cNW&UZW5o]Qf+*)hJnq)Cqlnb!AR4ee#^)-zL^J]Ik!BrK@tuz01rnxET<!Z+OC$5O}WFI?10$eoGyp@et<WJ8%:?^sF%Sv?gX&!vNR!HqyfQvDc}3cuAkv7NZm*&rq-lF/#K%Q$Ey)M-7a8k7DwpO7X=)IWiq1iCQ(JA#Y}cEEzp*H2L8GR*3!QI#@s$U}wM]JpXc0YsaWh2>P0T<{Vo@ZoQCmdT39a3&5rFN}cn{dgZ(grTe0<@mQ}85D/4X^r<VNNA?gB2r2N9EqQ>ld<qzF&-B=m
h*gl#WfTpZp?e}iPUtAS/XUo]zyUlw/H19PHM1vOA$E6+PQ0m+(%aI@sNtF<Un$1AgiOBDp7DH^WWC)[z:^#gVppA:Rr{*z5aD.%DkW1AXpENe:eVzj%7$!buA8=zl+)}^GXE*IoMH.bNKTKeH}}PwJjGLs*5QOTa^5gR(0/S#x1{mnOv^0+?!fMw)VcSbQ7BaTHf>peck+l}39KLH7T>{RJBpu$/j=Y&Q/VnL<d0e/c?l*fo8HGTekzvWAUSRtCU5kX4j)VMnnDB*Y/Bdfj>L-LB!SW4Hz8+TS?nV@gf20Rna7-H).ON#yzsiyWLBws
.^%.yA{UEhL#?vNdjZA5N:A]8TkvhhA{MHoeHEm0zz<p#w17M!j6uVNGJPL4E]y!bhuTk2ZeyR>l2OVEpbC.a!rfNCKy&+UPY%5b49aPs8F368-wtpYzfc2O.5hTVT5yJyYyy]oueP${?su<}.lp$lNA2NkpLk<bLRk>5JNG$]BV3&sN%Biar+w2XdYsvF<@<3@2XkQGJYs?hbL6zm3sZX}Uw>(@b^ck%c.9gWH?y5L)o5BPizx&Y0P$$-/?MgQ=Pk<@304V*.NUN8zX)VDOcc-c*(OJmj>HrZK!7x!cQzy<?8.VwvU@KtuXRtj>KM-@
VJTx:>^O&$OfWKVj(qh%IzQ#zSeHou:FtQ!K(yt>@JD!=]i<Scd3KQsG-)iCSkQy.j0!:c(KGmcEY&7?)qCLLE%[yJ>oMFgWEX)kKHDf=]at!<2a]EErnHzi?e8B#/w<usna8r8iArlpDZRh.pqWUnAk9SM3VKDoGu0&H8gHZ>?)7#E0inon/s%TJ?za(9?}NP-?AhLc2mPr=(Ua%by5wD*hYmA<jh5d7Og#@F?vPc<?k%C0ruRhcIDvmwWTxg2Ld99=gp:K!7]LP<D+K^vv(mN<ds222S<+efvz]HWb[}->J6xy#D4<w9z.QFjb?>Fc
j78%5).@uBus7qKYqoR}Kfo8FzI=9+GU[Ib=0X-.i{y}F7*M1z/-i$ygf)TqY3o[f!aknmhp<)r-rXxOwf0.:Mm6Oo1}^9OS}KlW::[xDHd]knt#CnBprqW1}&vl]E#WO<<H6kJ^h55Hqp1n2}pZ#LVF7YdYc}0$R25S%SUq97{.t4a>}Mp.oSRsw0AcPx1TzUtrj2*1<*b*xWG1-V0LKFpB-hV$@IJ{}Kq)g+=eSNw-mIA7}+G%eX-ep]U*W^p>D<s&{]!9V7DR24)b4To1<L@0/[Dm18@x%Wte2</N$}QBC5Mt5hS:{]CM3^9}Se]Q
C^)BY(!.J>K6]Ze}P)F0)(c<$s[ljffzaS@F{hXn+ywrqkHwgE@@}6P5vqrR[7c>6z]3)7)0/ij@z@Y=HU.*@Ymg7Ocbje6J28yu/V^#i%0Dd[VFTs7@i9[FL/ZQM6N?3ucC-{&UxFm1P.Z?rjO664Zxjm+gqwJGgW!fb]cmMV4i1?>@%JU8YXhmzQy=KwV&8SnpQa5NtDFI.-P54!=tl&#0pe:&EV5zXWKZyvs7HNAE4KPPZVI@DK:+AhB^XNYT$ujGdvl1)ov#mCee(BkDhI-[}.0lLr9&^K7Qi!Q*Vt{dDNE%H982nU?.h7MU:JPm
M*.CsO:D1eAGo^<mz3$zL9{w}TF]6q+yf.zx}zF&ZS8*6i:gpOFo}OU*s-p*NRz=wg&AWt-JbuGmh]LJRz][j>FQ0yn+A(d[Eft909l9g/prXq+}[^DMcR7G5%Dp=q0v()z(#lyhUi7:fPT+{YWSc3+7$JdrvUfW/UYGzBIB(.K$jF?YY3E2&=ogtS5#ZDD#+7sKu@Dc@s^@FyqhN2G5>epR41l^)2{r+cRPAw8^gvnB/@8!:-ZzI(kMUeuv8y:>=*yM1xE^b<>@P)uW.})W---Y+s}i#hL+Trs{q58+fl?gRb+f#}ut--FK=hvDBZ/!
]69M:+0?fFBG@igG+cGi]j?Y&X^j/VIpRynyKJl71uD8keEa9!iqNY+q%P^YL^%=n3Tx<btooB9h-N)>&f-V199.hYK<3[hZ2VPjstJ9<{$zgTW{L{BkZ6^gtjfGGfxCi${Q(b(FQ0VJd#^xo-z7k=BmW3=Cd.Y:Li{59Zwt^pYN*PyRKyKd9LsS0f?WaVuWq=KUE+{RC<5)Nj^qkan]8<mAQr{CXL3h4HPX1X)SRy1+tZs%keu-{W[qfm0{.gATCRCFQtq9[Z6#44yL!dLO&qRzz$xb2EQYEMbY&SL2!peq0cR7&t4AWAy<W!G&pAyL
!@TZUilf!R%a*T!tH9B42<YBL<]/(1A[=N@UZRM(nx/uteAe5B&X!$<G?c<5395R^({EWcya(g$6y&[BL@8&)(Zku]OO6}%UEQ4L]vd#]GL:>(6/i7{/-Dm%8{74V6vo}O+SvjH&nck!x0FQBD7}!-D=cln]dKG8G:A4Q<d6>[qHA6Il[lKfKw6h?TK-quXsPtJNaNQ=Bcdb/?lU+]Wljgm1*69w]1z*s:VV&CotT0Pc1>ABE<@RIu#AA$vZ9T>2=@(Wx-i]z2=%PDKZM}KMM)!9?+lQ++GMI@AN6Z]o2R9B<zywx!RD9Xh)V0y3cw?n
1#7RA<wEB6I<C9aboVX&26)F?0:Zya3)hf-En5f+8CkV]1fuhiOLG%MPq#zP&o:0K@cErONh@m)YdZk^>?dS8P67WPSDuNUvYs1&{gW^>3eRr({s@<=WwRZ0YBQd.Nq%lUek)re3#^UnQ3Q07g7qoxrn^n)h!sc%FH#dDj+j83a^j9soFuT.+0edNw&$L=(<1ZG4I}!=dV[Oy8O#eQq<o8*/e6?7328k0G)e[%L/9MQ[TVsFCQ+cqL4M*jyQ+4EE>Tg:CD-Q&lmQhWGEiI{&u+[bf^x/>FVC1Bn^nIaswy:%188j944Kf2AAD/w7vi3e
e5!2G&0>qTLi!^-qKG%+M:K&eC16cH(P/0mH.G&56q4lL+x6oYOyRuU/BCTro}R+wvy$s3/W+ZUpB0.U>jY*L71$j+n=BW{vx#XOWEY*+nB4/2)cs+c>+Ra&.>9iz2y$8REsyZMC&IkLkd]/9YV$}h=c)XXc(f&08PmfdTIXH47vv#%*X>9OFVH)aSUL1lfTz$g@LiO^gb%E0v3+2[>AmVFqe#NYy?u{qGhS/tXVhmzXmo%Tnhq#ZH=gS&Po>MTcJen9cf8ElV3mZabcj5s<ugqFmf2sveWrz9e0NOu^t&n[u=c+Yxb<*XhJ?snaJOPn
U:3d/6z*hG?<wqff7OG3ctvsvUbAhhVeH5R6Y4ic[aR=Ll7.5Jx}$6$==9844Z%ygK&)Kwt<yK1F:5qSh-r8dW?!xqLeuRCB8?f[zV=%yjTFb-Qsd6+G{?kw{PovHC@abpIA/4)J}SiWoOrRBRBA)15n!#4Mm?529s>k!1AGi>2Rqf^q#O@py-X:B=NtcO8784&@]wPPzb^f4i^ic6pU=no{k9U=5]S30Nt27xammuMQW{<GdC5jUlu]4w3?qhLBi!1RdEmyifwn-&l50w:HQnk&AT}RIapX>ae?g<MS@V9Gkl}c3hPnis&((OU&OL%s
q/zNHy4!6<?b(mklQv(^/-3W=!W8eCWN>7JV/nLFtI0Xt-08YMHjJ[WxIIps>mq/&G<shkdMQ6#dxYr&=xN@:{QTnhGKza4>ww!@z1Dn5K%1Y@V(r#3s^GI.I>oR=O5*^UKIkWpSybrbG#WUFmE]SCx}mVZzXLuW9?uyP6{wDbf6{%*mQ}#RvZKbO1VvWDlq1N$ZIp4+6f1$8BJCoWOkZegJASVn:nCIB[=!y5C2q7F[de+I4}SvN!HZB#2ZOW8ikYNt.aC2:Ci{2T.Hs?uoP$}?7I@4#=O!m0H#)9fSm<^-j8m)>q%j4]ZwFc5o.KeV
u!!SkcJLAr5:fECx6JNtWbz8>g3NZ>Tuk+@acS60O0)[D>7dd%CL##.Ao.0^U2pBW&0+sd]g}d%[[U(ODCRMVS0IHz)x42^l6pX2[jRZV.D$Y$M>5cTQ94^Mx9<upr*bc4/I!NhorH^^g6w-}jCGo^V5.Yu>i{fwtdg.{g(5r&O#H%WxZzAeN8@r$4/pajEpG[DXryEnH[[VLVOy:JngmiP?}Z:^h%s7KDRPIR)9>aM^{6/2vcpM?SkYL5O*T0&zo-3/C$Uo{A0QE/SM6+cuTt[0[Rl6Qj+Bx<AVFl?8yw6VpB.<Zeo?V7/#M{jPUb=F
.}@NP[Em#o6VBx[U8k#{E#NGUZbGM?U2y?aI(+q1UrY<VCB-e]=8MA#Z38lfXReW@]]3GSZTBi/^cNJqq-N*$PsswO/SAa9-pO-Y6UQopea(lpcHZybmu$cb8?R7Hr@uR99m=:=>gImdk#):qJWV:K02]XvnrT:<6/Vtj-rS/m?&$9oaek9bd0M{Dinx.I=VSTIby?ce6kL}EstW.]}k{d?c/%mlc)t]dTtbE0v8IoLIKx$6y&&zXkh/:{ftV%iy).$(u5>UhT.6(LefSJ#A/J#Xh+4PHrMh9zfC&RutZW6[=eoZq[Iiya7aq8gNh}va
>xcB=RQurBITi)8OQi.^YHRM2nNwvldYBFCdi0hoDjEasDKmFOFSEYl*BvWUhxfkx^WT[^j*7lvn=^TcurPyDVuW(6R(^%?}=^:G1kv[R8jM?@jEDF4?ZD[m9!dwOEa8//)nd]%OJWisltF9SUp-bzEK+t1/XS{IAEY)@Zf!*g4SAx6K.G8LNzj-<MeZlGi*z2PzKH2pz2U]6ya3G![rAbP+&3M4eu]c{z1Z>tze$.u7DaDn@kSsQ2c@tCjY9rfILU2*o)TbM&/b:WojY$1.1eR:oy>^xCBZslPL=79^R6KX39SE1TD@RI-V.iT-?</w
&pyhYL4J]$1>eQpAF@y7WOhsZxZtPpxKhFYsh&2$.niuliO9y4tm5vWh%-}60g@Nbn+}*pdu%qX=gn&(mRkJzqGo((K{&)yw!8^X[f.1Xb*Cd(G5].VfP){6LNJJ<KhByvL39%]+yblAAB}On2v!yFX&n8X?*?n]PG8D>1vxfEoF#K:dIwT>E{@ZL[Lj/djOyo8lMKX-CR2qdVPsJW47]8K=gtX*qqdiMazeAGy]u+P:>$qWwv0<8?8%L6HQl<U[2hTlxE6fskn=%*+sl<[C7UcJb7>?[4RzkxOMLRV?nj[yUR4al?j00!)aRo#U=xw5
8h}:5XE1H<}}Jcl0q15=KbO>-deJe+raCyk=ny)(.<r99kJtcJs}BYBGdS@GhVwwOAQn!x*s}Fyz]3sM9=]SZA0Pk]6uRJjSk5Bps.wO-w[0aQRoo/@D(@7gM)H]so}}9M.Dv!O5BQI%NNDM(d:$3H[(-RB+35U{e]/9QvRYvTN]$LeU?f}]vkVrJF&gK0l7PgCOn0?+siw!N!J]WqaU/GA/EJv]eQ:8Ah5$Fb4nNyJQ}hIwdPyBaDaUBbM6d.EeTM24V%V9(]v{D5JB#$204QH$nP3ma{s)hMonWD8ah@PaUjpGPO}5zTP>7V:*4v/#
/MkPBmHDJ{Gg-9pv0?bQ.Jnk5OVGG^HHB@1[EPEGe#OUS@zepR??6X4y4IF1j7^T(urlTVM9Ez0C)lENUQFk<N%#dvR6!=a+gbc4.{gdjLNojys$0R(XX9tf]IoI+b{x$hE{i/>iwn}}2q51Ff$%3663I.i>IvHPi?(Xp)my=Kxs]}2^U)&&BV8Fc}g>6<5O:.GIPORLj=}HvMZG!:*Xud1C2a!zFT)SJv>JutoY}8^ZX!l5fjr%=1X[.Mp$QF&zkjR&kDVvg0C(EGw2&<4]aD*M/<HU=-{msUBF4B#TnJK%O<Q*iC<3)?0<MPS]P6hN
@+F:&MRgT+zOcxk!AWV==I%Ss]OoW5nm7W!L]T1k4%$V+M+BMStqw(%=n6E}TrQAH:qA:WEJ4{>0}s$N1X]BLnMLc5lh}d$wekhK+EFXx)n7>REQkCu8[=6[iYXSBx)I=aErjHCUOPjM]3{kJw0bR^uJfI[Z<=K1n(3}UL>ieypxdg[]b1U1@Az6tT<bLkS}Vur?I=J4w>tG]t9AnZZx1i]/V^7mNW&VP7XhUhF2D7}RiZt0*-%{9?sGK{iNkcAqDIaWY]h<+]bDtB<J/xb(Y.tTLL>%VmxPqWn}74N@Fgk/>]%kVe>OtfYy[d=ZLq-O
DJi9TZ4Hol@v=Zm%nz:n)eK{3amf3z@@iI-%3#RU=+!M6?2v-rvQe.U5:$hW{j4a$z&0qf(vfPy?ivTn}#lTDWT*??T#$wq001r!moBUxvru6wv@=c?By+CcfFLNtfFUjmg/Mg/iX&MNgCyskgcd9yr2uZ80000BBvxqNwmoN>i=e!Cx>zR<gb7ByeIF}if/h&:hAAPJiX@DufFUWyfEoe=ZYjum07/QOm6n<:F=C=J
""".replace('\n','')
```
Sitten vielä rakennetaan Z85-dekooderi ja dekoodataan ylläoleva sotku.

```python
z85='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#'
z85dec = {}
for i in range(85):
    z85dec[z85[i]] = i

def decode_frame(s):
    n = 0
    decoded = []
    for i in range(5):
        n += z85dec[s[4-i]]*85**i
    for i in range(4):
        m = n // 256**(3-i)
        n = n - 256**(3-i) * m
        decoded.append(m)
    return decoded

def decode(s):
    decoded = []
    s = [s[n:n+5] for n in range(0,len(s),5)]
    for i in s:
        decoded += decode_frame(i)
    return bytes(decoded)

with open('cover.png','wb') as f:
    f.write(decode(cover_encoded))
```
Yksi merkki vastaa siis yhtä tavua, ja merkki muunnetaan tavuksi ottamalla sen Unicode-numero ja vähentämällä siitä 192. Valitsin numeron 192, koska sitä seuraavat 256 Unicode-merkkiä näyttävät suhteellisen mukavilta ruudulle tulostettuna.

### Interaktiivinen kirja

```python
def page(book,n):
    with open('.___location','w') as f:
        f.write(str(n/len(book)))
    for i in range(os.get_terminal_size()[1]):
        print('   '+book[n+i])

def main_loop():
    book = parse(code['markdown']+text)
    book = plaintext(book,os.get_terminal_size()[0]-6)
    for key in metadata:
        book = book.replace(key,metadata[key])
    book = book.splitlines()
    length = len(book)
    book = book + 10*os.get_terminal_size()[1] * ['']
    try:
        with open('.___location','r') as f:
            n = int(float(f.read())*len(book))
    except:
        n = 0

    if n > length:
        n = 0
    page(book,n)
    while True:
        if n > length:
            n = 0
            page(book,n)
        cmd = input('>>> ')
        if cmd == '':
            n += os.get_terminal_size()[1]-1
            page(book,n)
        elif cmd == 'edellinen':
            n += -os.get_terminal_size()[1]+1
            page(book,n)
        elif cmd == 'alkuun':
            n = 0
            page(book,n)
        elif cmd == 'seuraava-luku':
            n = n + 3
            while True:
                if book[n][0:3] != '## ':
                    n = n + 1
                else:
                    n = n-2
                    page(book,n)
                    break
        elif cmd == 'käännä-pdf':
            build_pdf(tex_main)
        elif cmd == 'build':
            document = parse(text)
            build_html(document)
            build_plaintext(document,50)
            build_latex(document)
            build_pdf()
        elif cmd == 'lopeta':
            exit()
        else:
            print('   Tuntematon komento!')
```

### Pääohjelma

```python
def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == 'build':
            document = parse(text)
            build_html(document)
            build_plaintext(document,50)
            build_latex(document)
            build_pdf()
        if 'html' in sys.argv:
            build_html(parse(text))
        if 'txt' in sys.argv:
            build_plaintext(parse(text),50)
        if 'pdf' in sys.argv:
            build_latex(parse(text))
            build_pdf()
        if 'nimi' in sys.argv:
            import random
            print(random.choice(names))
        #elif sys.argv[1] == 'txt':
            #document = parse(text)
            #with open('prosessi.txt','w') as f:
                #f.write(plaintext(document))
        #else:
            #print('EPÄVALIDI ARGUMENTTI!')
    else:
        main_loop()
if __name__ == '__main__':
    main()
```

### LaTeX-aihio

```latex

\documentclass[a5paper]{{memoir}}

% Preamble

\usepackage[USenglish,finnish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{microtype}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage[hyphens]{url}
\usepackage{wallpaper}
\usepackage{listings}
\usepackage[colorlinks=true,urlcolor=dblue,linkcolor=dblue,linktocpage=true]{hyperref}

\definecolor{dblue}{rgb}{0,0,0.4}

\newcommand{\linkki}[1]{\footnote{\url{#1}}}
\newcommand{\vmq}[1]{\rlap{#1}\,}

\setlrmarginsandblock{0.7in}{*}{1}
\setulmarginsandblock{0.7in}{*}{1}
\setlength{\headsep}{0.2in}
\checkandfixthelayout

\renewcommand{\cftdot}{}
\setlength{\cftparskip}{0.22em}

% Etusivut

\begin{document}

\thispagestyle{empty}

\ThisCenterWallPaper{1.04}{cover.png}

\begin{center}

\resizebox{.5\textwidth}{!}{\HUGE\textbf{BookTitle}}

\rule{.5\textwidth}{0.015in}

\vspace{0.07in}

\resizebox{.5\textwidth}{!}{\textsc{BookSubtitle}}

\vspace{0.5in}

{\large BookAuthor}

\end{center}

\newpage

\thispagestyle{empty}

\begin{center}

\resizebox{.5\textwidth}{!}{\HUGE\textbf{BookTitle}}

\rule{.5\textwidth}{0.015in}

\vspace{0.07in}

\resizebox{.5\textwidth}{!}{\textsc{BookSubtitle}}

\vspace{0.5in}

{\large BookAuthor}\linkki{AuthorURL}

\vspace{2.5in}

\end{center}

\noindent Versio {VersionNumDecimal} ModifiedOrNot

\vspace{0.15in}

\noindent DateLong

\vspace{0.15in}

\noindent \texttt{CommitId}

\vfill

\noindent {\Large \url{BookURL}}

\vfill

\noindent{\copyright} CopyrightPeriod CopyrightHolder

\vspace{0.15in}

\noindent Kirjaa saa käyttää LicenseName -lisenssin\linkki{LicenseURL} ehtojen mukaisesti.

% Pääsisältö

\mainmatter
\tableofcontents

BookContent

\end{document}

```

### HTML-aihio

<!--HTML-template-->
```html
<!doctype html>
<html lang='fi'>
<head>
<title>BookTitle—BookSubtitle</title>
<meta charset='utf-8' />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="BookAuthor" />
<meta property="og:site_name" content="BookTitle—BookSubtitle" />
<meta property="og:title" content="BookTitle—BookSubtitle" />
<meta property="og:url" content="BookURL" />
<meta property="og:type" content="website" />
<link rel='stylesheet' type='text/css' href='normalize.css' />
<style>
StyleSheet
</style>
<script>
JavaScript
</script>
</head>

<body class='day' onload='lightsOnCookie()'>

<h1>BookTitle</h1>

<p class='subtitle'>BookSubtitle</p>

<p class='bookauthor'>BookAuthor</p>

<pre style='font-size:70%;line-height:2.0;white-space:pre-wrap;word-wrap:break-word;'>Versio <span class='sha'>VersionNumDecimal</span>ModifiedOrNot
<span class='shb'>DateLong</span>
<span class='shc'>CommitId</span>
<a href='SourceURL/commit/CommitId'>Viimeisin muutos</a>: <span class='shd'>LastChange</span>
<a href='./prosessi.pdf'>pdf</a> | <a href='#paperikopio'>paperikopio</a> | <a href='SourceURL'>lähdekoodi</a> | <a href='javascript:void(0)' onclick='lightswitch()'>yötila</a></pre>

BookContent

<hr />

<p>Copyright &copy; 2019 <a href='AuthorURL'>CopyrightHolder</a></p>

<p>Tekstiä saa käyttää <a rel='license' href='LicenseURL'>LicenseName</a> -li&shy;sens&shy;sin eh&shy;to&shy;jen mu&shy;kai&shy;ses&shy;ti.</p>

</body>

</html>
```

Sitten vähän tyylin säätämistä

<!--stylesheet-->
```css
body {
    max-width:500px;
    line-height:1.5;
    font-family:Serif;
    margin:40px auto;
    padding:0 17px;
}
p {
    text-align:justify;
}
pre {
    padding:10px;
    border-radius:6px;
    overflow:auto;
}
code {
    padding-top:4px;
    padding-bottom:4px;
    padding-left:6px;
    padding-right:7px;
    border-radius:4px;
    overflow:auto;
}
img {
    width:100%;
}
dt {
    font-weight:bold;
}
h1 {
    font-size:200%;
    text-align:center;
}
.subtitle {
    font-size:110%;
    text-align:center;
    font-weight:bold;
    padding-bottom:10px;
}
.bookauthor {
    text-align:center;
    padding-bottom:12px;
}

h2 {
    font-size:120%;
    padding-top:20px;
}
h3 {
    font-size:110%;
    padding-top:20px;
}
h4 {
    font-size:100%;
}
li {
    padding-top:2px;
    padding-bottom:2px;
}
a:link {
    color:#3377ff;
}
a:visited {
    color:#3377ff;
}

a.unclickable {
    pointer-events:none;
    cursor:default;
    color:inherit;
    text-decoration:inherit;
}

.comment {
    color:grey;
    font-size:90%;
}

.day {
    background-color:#fff;
    color:#000;
}
.night {
    background-color:#000;
    color:#fff1ce;
}

.night pre {
    background-color:#222222;
}
.day pre {
    background-color:#f6f3f3;
}

.night code {
    background-color:#222222;
}
.day code {
    background-color:#f8f3f3;
}
.day pre {
    background-color:#000;
    color:#fff1ce;
}

.sha {
    color:yellow;
}
.shb {
    color:cyan;
}
.shc {
    color:lightgreen;
}
.shd {
    color:magenta;
}

```
Kaverini kamihekun tekemä javascript-nightmode

<!--nightmode-->
```javascript
/**
 * nightmode.js by kamiheku
 */

/**
 * Gets and returns the classname for the body element
 */
function getBodyClassName() {
  return document.getElementsByTagName("body")[0].className;
}

/**
 * Gets and returns the cookie that contains the user's preference for
 * nightmode
 */
function getCookie() {
  return document.cookie.replace(/(?:(?:^|.*;\s*)lights\s*\=\s*([^;]*).*$)|^.*$/, "$1");
}

/**
 * Run on page load; checks if user prefers night or day mode and sets the
 * class for body accordingly
 */
function lightsOnCookie() {
  var lights = getCookie();

  document.getElementsByTagName("body")[0].className = (lights === "on") ?
    "night" : "day";
}

/**
 * Run on clicking the lightswitch; toggles the day/night class for body and
 * on/off for the cookie
 */
function lightswitch() {
  var name = getBodyClassName();
  var lights = getCookie();

  document.getElementsByTagName("body")[0].className = (name === "day") ?
    "night" : "day";
  document.cookie = (lights === "on") ?
    "lights=off;path=/" : "lights=on;path=/";
}
```

```markdown

# BookTitle—BookSubtitle 

Versio VersionNumDecimal (CommitId)ModifiedOrNot

DateLong

Hei! Tervetuloa lukemaan kirjaa *BookTitle—BookSubtitle*. Pääset eteenpäin antamalla tyhjän komennon (eli yksinkertaisesti painamalla enteriä). Tarjolla on myös komennot “edellinen,” “alkuun,” “suraava-luku,” “build” ja “lopeta.”

BookURL

(C) CopyrightPeriod CopyrightHolder

LicenseName 

## Esipuhe

```

!MainTextBegins!

## Tragedia

Miia ja hänen kaksi tytärtään olivat asuneet Miian omistaman vanhan paritalon vasemmassa päädyssä jo kuukausia ilman vuokralaista naapurina. Sitten eräänä päivänä asuntoa saapui katsomaan mies nimeltä Sampo. Sampo oli tiennyt muuttavansa oikeaan päätyyn oikeastaan jo ennen esittelyä, mutta Miian kanssa jutustellessa hän sai vuokraamiselle vielä yhden syyn lisää: aivan heti heidän tavattuaan hän alkoi tuntea vahvaa vetoa kohti Miia.

Paritalon ulkokuori oli ränsistynyt, ja Sampon kanssa Miia sai vihdoin maalattua sen uuteen uskoon. He tekivät muitakin korjaushommia yhdessä. Heillä oli hauskaa ja he tutustuivat ja lähentyivät nopeasti. Sampo rakastui korviaan myöten, ja hänen mielessään asianlaita oli selkeä: Sampo ja Miia eläisivät elämiänsä ja kasvattaisivat lapsiansa nyt yhdessä, perheenä. Siihen suuntaan asiat näyttivät Samposta kehittyvän, eikä hänen mielensä kyennyt näkemään mutkia matkassa.

Miia huomasi pian, että vauhti oli hänelle liikaa. Eihän hän tuntenut Sampoa vielä juurikaan, eikä hän muutenkaan kokenut olevansa valmis vakavampaan ihmissuhteeseen sillä hetkellä. Tilannetta ei helpottanut Sampon hurjan nopeasti syttyneet, valtavat tunteet ja hänen täysin jarruttelematon, suorastaan vapaapudotusmainen syöksyminen suhteeseen. Niinpä Miia alkoi ottaa etäisyyttä Sampoon. Hän teki sen pääasiassa tiedostamattomasti, ikään kuin hän olisi laskenut ohuen verhon heidän välilleen.

Rämäpäiseltä Sampolta oli aina puuttunut jokin ruuvi päästä, eikä hän ollut hyvä sympatiseeraamaan varovaisuutta. Sampo huomasi välittömästi, että jokin oli pielessä, mutta hän ei ymmärtänyt, mistä oli kyse. Hänen päähänsä ei mahtunut ajatus siitä, että asiat voisivat kehittyä liian nopeasti.

Melkein koko Sampon maailma horjahti. Hänestä tuntui siltä, että hän tuli Miian kanssa toimeen paremmin kuin kenenkään toisen ikinä vastaan tulleen ihmisen kanssa. Hänestä tuntui, että Miia ymmärsi häntä paremmin kuin kukaan muu, ja toisin päin. Hän tunsi sen taian, mistä rakkauslaulut kertovat, eikä mikään hänen maailmassaan tuntunut yhtä todelliselta. Ja kuitenkaan Miia ei kokenut asiaa ollenkaan samalla tavalla. *Kuinka tämä on mahdollista?* Sampo ei nähnyt asialle kuin kaksi varteenotettavaa selitystä: joko Miia oli sokea tai hän itse näki näkyjä.

Oli Miia sokea tai ei, hän ei nähnyt sitä mitä Sampo näki, eikä hän ymmärtänyt Sampon kysymystä. Hänelle tuli Sampon tunteilusta lähinnä vaikea olo. Hän ei kyennyt ottamaan Sampon tunnetta vastaan ilman, että hän olisi kokenut painetta tuntea itse edes suurin piirtein samalla tavalla. Ja kun hän ei tuntenut, hän ei tuntenut. Muutenkin Sampon rakastuminen vaikutti kummalliselta. Joskus se tuntui typerältä ja lapselliselta, joskus taas epäaidolta ja valheelliselta.

Sampolle kävi selväksi, ettei rakkaustarinaa ollut tulossa, ja hän kieriskeli tuskassa. Hän ei kuitenkaan voinut olla kuulematta sisäistä ääntään, joka sanoi, että Miia on tärkein ja kaunein asia maailmassa. Jos tuo ääni on väärässä, ei mikään ole totta, Sampo ajatteli. Hän päätti, että hän haluaa olla Miian apuna kaikessa, missä hän voisi olla, vaikka Miian tunteet eivät olisikaan samanlaiset. Sampolle kaikki oli taas suoraviivaista: hänellä oli ylimääräistä aikaa ja hän välitti Miiasta, ja Miialla oli yksinhuoltaja-yksityisyrittäjänä hurjan paljon stressiä ja työtaakkaa, joten tulevaisuudessa hän auttaisi Miiaa paljon.

Miiasta tämä tuntui vain entistä kummallisemmalta. Hänhän oli juuri tehnyt Sampolle selväksi, että suhdetta ei ole, ja Sampo oli sen mukamas ymmärtänyt. Silti Sampo käyttäytyi aivan, kuin he olisivat olleet parisuhteessa. Hän ei tajunnut, että “ei suhdetta” tarkoitti etäisyyttä, omaa tilaa; sitä, että kukin pitää huolen omista asioistaan. Tilanne alkoi tuntua Miiasta ahdistavalta.

Samposta tuntui entistä pahemmalta. Hän ei halunnut Miialta mitään muuta kuin sitä, että hän saisi auttaa Miiaa arjen taakan kanssa. Hänen sydäntänsä kivisti, kun hän näki, kuinka Miia ponnisteli yksin suurten vaikeuksien kanssa. Sampo olisi voinut tehdä niin paljon asioiden helpottamiseksi. Mutta hänen apunsa ei kelvannut. Sampo alkoi vakavasti epäillä omaa kelpoisuuttaan ihmisenä. Jos hänen apunsa ei kelpaa, vaikka hän ei halua mitään vastineeksi ja toinen tarvitsisi apua ja, on hänen oltava aivan kelvoton ihminen.

Sampo oli aina ollut avoin tunteistaan ja ajatuksistaan, ja yksi hänen vankoista periaatteistaan oli, että puhuminen kannattaa. Hän yritti puhua Miian kanssa yhä uudestaan ja uudestaan: hän yritti ymmärtää Miiaa, ja hän yritti saada Miian ymmärtämään sen, ettei hän halunnut omistaa tai sitouttaa Miiaa tai kontrolloida hänen elämäänsä; että hän vain välitti Miiasta kovasti ja halusi auttaa häntä.

Jokainen keskustelu vain pahensi tilannetta. Miiasta tuntui, ettei Sampo ymmärrä häntä millään. Hänestä tuntui, ettei Sampolle riitä mikään, ja että hän on omana itsenään aivan kelvoton ihminen. Hänen tuntemus oli aivan sama kuin Sampollakin. Hän alkoi ahdistua Sampon kanssa juttelemisesta jo etukäteen, mikä vain huononsi keskusteluja.

Sampo näytti kaiken tuskansa Miialle. Tunteiden näyttäminen on hyvästä, hän ajatteli; sehän on vain selkeää viestintää. Jos tunteita ei näytetä, eivät ihmiset voi ymmärtää toisiaan. Mutta Miia ei kyennyt mitenkään ottamaan Sampon tuskaa vastaan—varsinkaan, kun tuska kumpusi siitä, että Miia ei kokenut luontevaksi ottaa Sampolta apua vastaan. Miia ahdistus kasvoi ja kasvoi. Pahinta oli, että Miia tiesi, ettei Sampo tarkoittanut mitään pahaa. Se sai Miian kokemaan syyllisyyttä tilanteesta.

Eräänä päivänä Miia romahti. Onneksi hänellä oli hyvä kaveri lähettyvillä. Hän selosti kaverilleen tilanteen. Hän kertoi, kuinka Sampo vei häneltä kaikki voimat, ja kuinka samassa talossa asuminen piti kuorman kokoajan läsnä. Miian kaveri otti yhteyttä Sampoon ja selosti, mitä Miia oli hänelle kertonut. Sampolle tuli hirveä olo. Hän ymmärsi jotakin, mitä hän ei ollut aikaisemmin tajunnut ollenkaan. Hän ymmärsi, kuinka hänen halunsa auttaa oli saanut aikaan paljon enemmän pahaa kuin hyvää. Hän suri sitä, ettei Miia ollut kyennyt sanomaan asiaa hänelle itse. Miia taas hämmästeli sitä, kuinka Sampo tajusi asian vasta siinä vaiheessa, kun joku kolmas henkilö toimi viestin välittäjänä. Mutta hän oli valtavan helpottunut.

Sampo ei kyennyt muuttumaan hetkessä tarpeeksi paljon viisaammaksi, ja hänen läsnäolonsa jatkoi Miian kuormittamista. Eräänä päivänä hän muutti raskain mielin paritalosta kaupungin vuokrayksiöön. Siellä hän vailla muuta tekemistä pohdiskeli kaikkea tapahtunutta. Hän opetteli ajattelemaan, että parasta, mitä hän saattoi tehdä Miialle, oli mennä pois. Hän oppi ajattelemaan, että hänen välittämisensä oli jotenkin perustavanlaatuisesti viallista. Se, että hän rakastaa, on huono asia. Sampo menetti uskonsa itseensä kokonaan.

Aikaa kului, ja Miia löysi toisen miehen. Sampo löysi alkoholin. Tyhjät pullot kerääntyivät hänen likaisen yksiönsä lattialle. Aina silloin tällöin hän avasi vahingossa Instagramin ja näki siellä onnellisia kuvia Miian uudesta elämästä. “Ei tuo mies voi välittää Miiasta yhtä paljon kuin minä,” hän ajatteli. Ja heti perään hän muisti, että hänen välittämisensä on jotenkin viallista. Sampo avasi uuden pullon.

### Ydinopetukset

Olen käynyt läpi vaikean vaiheen. Jouduin uudenlaisiin, vaikeisiin tilanteisiin, joissa en osannut toimia oikein. Tein virheitä ja aiheutin ongelmia sekä itselleni että muille. Itseuskoni kärsi pahan kolauksen. Mutta virheistä oppii, ja nyt olen viisaampi ihminen. Pyrin kirjoittamaan tähän kirjaan tärkeimmät oppimani asiat.

Kirjan kohdeyleisö jakautuu kahteen osaan: niihin, jotka uskovat itseensä vahvasti, sekä niihin, jotka uskovat itseensä heikosti. Nämä ovat itseasiassa yksi ja sama joukkio, sillä ihmisyksilön itseusko ei ole suoraviivainen asia, ja sen vahvuus vaihtelee riippuen siitä, kuinka läheltä ja mistä suunnasta ytimen ympäriltä häntä tarkastelemme. Jokainen uskoo johonkin itsensä osaan vahvasti ja johonkin heikosti.

#### Jos uskot itseesi heikosti

Sinulla on vaikeuksia uskoa, että voisit olla hyvä ihminen toisille. Jos joku näyttää arvostavan sinua, oletat erehtyneesi tai joutuneesi huijauksen uhriksi ja toimit sen mukaan. Sinulle on luultavasti tapahtunut jotakin kauheaa menneisyydessä. Ehkä tiedät, mistä on kyse, tai sitten et muista asiaa, tai et edes osaa erottaa sitä historiastasi. Kyse voi olla selkeän väkivaltaisesta tapahtumasta tai rakkaudettomasta tai hyväksikäyttävästä suhteesta, tai huomaamattomasta, lähes normaalina pidetystä nykymaailman jatkuvasti läsnäolevasta ominaisuudesta, joka lyttää ihmisyyttä.

Historiasi opettanut sinut uskomaan, että olet huono ihminen. Mitä varhaisemmassa vaiheessa opetus on tapahtunut, sitä vahvempaan se on uskomuksesi juntannut. Kun uskot olevasi huono, et oleta ansaitsevasi arvostusta, ja tämä näkyy ihmissuhteissasi. Hyvilläkin ihmisillä on vaikeuksia arvostaa sinua siitä yksinkertaisesta syystä, että et uskalla antaa itsesi puhjeta kukkaan heidän edessään. Saat kokemuksia, jotka vahvistavat käsitystäsi huonoudestasi.

Uskomuksesi omasta huonoudestasi voi näyttäytyä monessa eri muodossa riippuen siitä, millaisia kamaluuksia maailma on sinulle tehnyt. Saatat esimerkiksi pelätä, että sinut aina loppujenlopuksi hylätään, kun toiselle paljastuu, millainen ihminen olet, tai kun toinen väsyy olemaan kanssasi säälistä. Takerrut toiseen ja yrität epätoivoisesti pitää suhdetta pystyssä, aivan kuin se olisi kokoajan vaakalaudalla. Pelästyt tavallisia hetkellisiä erossaolotilanteita ja toisen normaalia kaipuuta omalle tilalle. Epäilet, että toinen ei ole rehellinen sinulle; että hänen välittämisensä on epäaitoa joko siksi, että hän säälii sinua eikä raaski kertoa sinulle totuutta, tai siksi, että hän haluaa käyttää sinua hyväkseen. Saatat raivostua, kun koet saaneesi toisen kiinni epärehellisyydestä. Saatat itse laittaa välit poikki, joko välttääksesi pelkäämääsi hylätyksi tulemisen, tai sitten ollaksesi reilu toiselle: ethän halua olla riippakivi, joka vain kuormittaa toista.

Saatat pelätä, että sinua kohdellaan loppujen lopuksi aina kaltoin. Lyödään, petetään, käytetään hyväksi. Pelon vaikutus voi olla päinvastainen, kuin hylkäämistä pelkäävän ihmisen tapauksessa: et päästä ketään lähellesi. Et avaudu ihmisille, etkä juuri näytä tunteitasi henkilökohtaisissa tilanteissa. Ehkä kammoat kosketusta. Jos päästät vahingossa jonkun lähellesi, saatat säikähtää ja laittaa yhteyden kiinni kuin veitsellä leikaten. Keskityt suhteissasi pinnalliseen, vähemmän vaaralliseen kommunikointiin. Sinulla ei juuri ole todellista yhteyttä toisiin ihmisiin, ja niinpä koet yksinäisyyttä, vaikka sinulla olisikin paljon pintapuolisesti hyvältä näyttäviä ihmissuhteita.

Hylätyksi ja kaltoin kohdelluksi tulemisen pelkojen kaltaisia elämää haittaavia uskomuksia eli niinsanottuja skeemoja tai tunnelukkoja tunnetaan kirjallisuudessa useita. Esimerkiksi osoitteesta <https://www.tunnelukkosi.fi/tunnelukot> löytyy kahdeksantoista tunnelukkoa kuvauksineen. Listalla ovat muunmuassa alistuminen, emotionaalinen estyneisyys, epäonnistuminen, hylkääminen, hyväksynnän haku, kaltoin kohtelu, tunnevaje, uhrautuminen, ulkopuolisuus ja vajavuus.

*Tunnelukko* on ongelmallinen sana, sillä se antaa ymmärtää, että jokin tunne olisi lukossa sillä tavalla, että ihminen ei kykene tuntemaan sitä. Toki tunnelukko usein näyttäytyy tällaisena lukkiutumisena, jonkinlaisena pelon aiheuttamana emotionaalisena halvauksena, mutta halvaus on vain tunnelukon oire. Tunnelukko on omaa itseä koskeva uskomus, ja se voi olla olemassa ilman emotionaalista halvausta. Englanninkielinen vastine *life trap* on paljon parempi sana. Mutta Suomessa *tunnelukko* on levinnyt laajaan käyttöön, ja niinpä kirjoitan itsekin tunnelukoista.

Melkein kaikki tunnelukot toteuttavat itseään, sillä tunnelukkoa rikkova tilanne tuntuu epäaidolta. Jos olet tottunut alistumaan toisten tahtoon ja tekemään asioita sen perusteella, mitä arvelet toisten haluavan, et voi uskoa, että joku joskus todella haluaisi kuulla, mitä sinun ikioma äänesi sanoo sinun haluavasi. Et anna äänesi kuulua, vaikka toinen sitä haluaisi. Ehkä pelkäät, että toinen vain esittää haluavansa kuulla sinua. Tai sitten pelkäät, että todellinen äänesi on niin kammottava, että toinen vain luulee haluavansa kuulla sen. Et jätä toiselle muuta vaihtoehtoa, kuin jyrätä sinut hänen omalla äänellään. Tilanne palaa siihen, että teet asioita toisen mieliksi, toisen ehdoilla.

Skeemat saattavat toteuttaa itseään myös toisenlaisista syistä. Hylkäämistä pelkäävä tulee helposti hylätyksi, koska toinen ei jaksa jatkuvaa pelon aiheuttamaa stressiä ja temppuilua. Kaltoin kohtelua pelkäävä saattaa tulla satutetuksi vahingossa, kun toinen ei ymmärrä pelkoa ja odottaa ja ehkä käytännössä vaatii samanlaista rohkeutta kuin mihin hän itse pystyy.

Heikko itseuskosi satuttaa ympärilläsi olevia ihmisiä. Et ymmärrä, että toiset tarvitsevat sinua ja hyväksyntääsi aivan, kuten sinä tarvitset heitä. Et käsitä, että toiset nauttivat läsnäolevasta seurastasi ja kaipaavat mielipiteitäsi ja näkemyksiäsi. Kun sulkeudut tai vetäydyt toisilta, viet heiltä kauniin ihmisen pois, ja se tuntuu heistä pahalta. Kipu on suurta erityisesti, jos sulkeutumisen tasosi vaihtelee radikaalisti. Kaikki kipu johtuu siitä, että muut pitävät sinua parempana ihmisenä, kuin minä itse pidät itseäsi. Et ymmärrä, mitä viet heiltä pois.

Et tietenkään ymmärrä aiheuttavasi kipua: jos ymmärtäisit, et aiheuttaisi sitä. Mutta toisista tilanne voi näyttää siltä, että sinä viis veisaat heidän tunteistaan. Toiset saattavat pitää sinua itsekeskeisenä ja jopa kylmänä tai ilkeänä ihmisenä. He saattavat reagoida syyllistävästi tai olla takaisin ilkeitä sinulle. He saattavat myös kyllästyä sinuun ja alkaa karttaa sinua.








joudut helposti sekä ihmisten että organisaatioiden huijaamaksi, koska et uskalla luottaa sisäiseen ääneesi, ja koska huijarit osaavat tarjota sinulle juuri sitä, mitä kipeimmin kaipaat. Huijarit osaavat myös taitavasti olla herättämättä pelkojasi, jos niin haluavat.










Kaikki ongelmasi johtuvat siitä, että olet syvällisellä tavalla väärässä: todellisuudessa et ole huono ihminen. Sinä ja ympärilläsi olevat ihmiset törmäävät jatkuvasti tähän epätotuuteen, joka elää itsepintaisesti mielessäsi, ja se aiheuttaa kaikenlaista harmia.

Vika ei ole sinussa, eikä mikään tästä ole sinun syytäsi. Syy on kylmässä maailmassa ja niissä ihmisissä, jotka uskottavasti valehtelivat sinulle, että sinä olet huono ihminen.

% Et ehkä halua ajatella, että pidät itseäsi huonona ihmisenä, sillä itsevarmat tai itsevarmaa teeskentelevät ihmiset ympärilläsi ja sosiaalisessa mediassa 

% Saatat uskoa, ettet kykene ajattelemaan. Jätät mielipiteesi sanomatta, tai oikeastaan et uskalla edes olla mitään mieltä. Jos sinulta vaaditaan näkemystä, kopioit toisen näkemyksen. 

#### Jos uskot itseesi vahvasti

Uskot olevasi hyvä ihminen ja uskot, että sinusta on apua muille. Luultavasti sinulla oli rakastava perhe, joka piti sinusta huolta, arvosti sinua, antoi sinun olla oma itsesi ja jakoi aikaa kanssasi. Olet selvinnyt elämästäsi joutumatta pitkään murskaavaan ihmissuhteeseen tai kylmän, mekaanisen, kasvottoman kulttuurin liian kovaan puristukseen. Sinulla on ollut hyvä tuuri, kun elämän pelikortteja on jaettu. Senkin onnekas paskiainen! Et tiedä elämästä mitään. Sietäisit saada turpaasi.

Kolme edellistä lausetta ovat huumoria, mutta niissä on kuitenkin pisara totuutta. Vahva itseuskosi antaa sinulle rohkeutta, ja mikäli et samaan aikaan ole tavattoman viisas ihminen, on rohkeutesi uhkarohkeutta. Et pelkää sellaisia asioita, mitä ihmiset yleensä pelkäävät, ja niinpä rikot rajoja ja aiheutat hämmennystä. Loukkaat ja pelotat toisia. Vastaukseksi saat hyljeksintää tai vihaa tai molempia.

Katsotaan tarkemmin, millaisiin ongelmiin saatat joutua. Uskallat näyttää tunteitasi ja puhua asioista tavanomaista avoimemmin. Tämä saattaa tuntua toisesta epäilyttävältä, sillä monesti oikein vuolas ja syvällisenoloinen tunneilmaisu on epäaitoa huijausta. Jos toinen säikähtää tunteiluasi tällä tavoin, sulkee hän yhteyden sinuun. Muutos voi olla yhtäkkinen. Mitä oikein tapahtui?

Toinen ongelma tunteiden voimakkaassa ja avoimessa ilmaisemisessa on, että se luo toiselle paineen ottaa ne vastaan ja reagoida niihin. Tunteen näyttäminen on pyyntö lähteä mukaan emotionaaliseen dialogiin, ja jos toisella ei ole siihen resursseja tai rohkeutta, saa tunteen näyttäminen toisessa aikaan riittämättömyyden tunnetta.

Koska uskot omaa sisäistä ääntäsi, saatat nähdä läpi epäaidoista, manipuloivista, vaarallisista ihmisistä. Kun näet tällaisen ihmisen koukuttavan sinulle tärkeää ihmistä, haluat varoittaa. Kohtaat lähes varmasti kieltävän, ehkä vihamielisen reaktion, sillä varoittamasi ihminen on jo jonkinlaisessa koukussa. Kun varoitat huijarista, saattavat muut jopa luulla, että se olet sinä, joka huijaa. Varoittaminen kuulostaa helposti tarkoitukselliselta mustamaalaamiselta ja eripuran ja epäluulon kylvämiseltä.

Sama pätee muihinkin huijauksiin: uskonnollisiin kultteihin, pyramidihuijauksiin, toimimattomiin terveystuotteisiin ja hoitoihin, epärehellisiin poliittisiin puolueisiin ja niin edelleen. Näet näistä läpi ja haluat varoittaa ystävääsi, mutta hän on jo koukussa ja tulkitsee varoituksesi vihamieliseksi yritykseksi mustamaalata jotakin taivaallisen hyvää asiaa. Ystäväsi voi kapinoida sitoutumalla entistä voimakkaammin, joten varoitusyrityksesi voi viedä ystäväsi vain syvemmälle huijaukseen.

Kulttuurimme on monessa mielessä huijausta. Teemme asioita vain siksi, että että toiset tekevät niitä. Näet monet kulttuurilliset normit järjettöminä ja haluat rikkoa niitä. Tämä luo ahdistavaa ja pelottavaa kaaosta. Kulttuurillisten normien rooli on pitää yllä järjestystä ja saada ihmisyhteisö toimimaan synkronoidusti yhteisen päämäärän eteen, ja niiden rikkominen rikkoo yhteisöä. Tämä pitää paikkansa, olivatpa normit kuinka järjettömiä ja tuhoisia tahansa.

Normien rikkominen voi olla myös epäilyttävää, sillä yleensä niiden rikkominen hävettää ja pelottaa ihmisiä. Normeja rajusti rikkova voi vaikuttaa ihmiseltä, joka ei välitä mistään ja jolla ei ole empatiaa eikä omaatuntoa, vaikka totuus voi olla täysin päinvastainen.

Tapasi rikkoa normeja saattaa näyttää muista siltä, että pidät itseäsi erityisenä. Aivan kuin sinulla olisi erityisoikeuksia. Itse tietysti ajattelet, että kyllä muutkin saisivat rikkoa normeja aivan samalla tavalla, he vain eivät halua niitä rikkoa. Totuus kuitenkin on, että ihmiset noudattavat normeja, koska he kokevat, että heillä on siihen velvollisuus. Normien noudattaminen vaatii aina uhrauksia, ja kun rikot normeja, halvennat toisten tekemiä uhrauksia.

Itseensä uskominen on pitkälti sama asia kuin toisiin ihmisiin uskominen. Kun uskot itseesi, haluat auttaa toisia ja saatat laittaa toisten edun oman etusi edelle. Jos toisella ihmisellä on vaikeuksia uskoa itseensä, voi halusi auttaa tuntua hänestä epäilyttävältä. Se, että haluat auttaa, voi johtaa toisen säikähtämiseen ja yhteyden heikkenemiseen.

Toinen voi säikähtää myös siksi, että avusta on käytännössä aina maksettava jollakin tavalla. Jos ei muuten, niin edes hymyllä tai kiitoksella. Suuren avun ottaminen vastaan voi tuntua vaaralliselta velkaantumisen riskin takia. Saatat haluta auttaa pyyteettömästi, mutta toisen on mahdotonta tietää, onko asia todella niin.

Haluat auttaa voi tuntua toisesta pahalta myös siitä siksi, että se antaa ymmärtää, ettei toinen pärjää yksin. Jos toinen kokee olevansa riittämätön siksi, ettei hän pärjää yksin, voi auttamishaluinen ihminen vain pahentaa tuota riittämättömyyden kokemusta.

Vaikka luulisit haluavasti auttaa pyyteettömästi, ei asia välttämättä todellisuudessa ole niin. Et ehkä kaipaa vastineeksi mitään konkreettista, mutta luultavasti haluat, että toinen ottaisi hyväksyisi apusi ja arvostaisi sitä. Voi olla, että näiden asioiden odottaminen on liikaa.

Jos sinulla on tällaisia ongelmia, olet liian rohkea ihminen ja uskot itseesi liikaa siihen, kuinka paljon ymmärrät toisia ihmisiä. Ydinongelmasi on se, että et kykene samaistumaan siihen, mitä on olla pelokas ihminen. Samalla tavalla fyysisesti huippukuntoisen ja terveen ihmisen on vaikea ymmärtää jotakin päällepäin näkymätöntä toimintakykyä rappeuttavaa kroonista sairautta sairastavaa ihmistä.

Rohkeutesi yhdistettynä kyvyttömyytesi samaistua pelokkuuteen alleviivaa toisten pelkoja. Ongelma on kaksin verroin pahempi, jos olet ylpeä rohkeudestasi ja haluat näyttää muille, että uskallat tehdä asioita, joihin he eivät pysty. Tällöin käytännössä osoitat sormella toisia ja huudat: “Sinä olet heikko!”

Ongelman vakavuus kaksinkertaistuu toisen kerran, jos alat huomata, että toiset ovat pelokkaita, ja alat syyllistää heitä siitä. Syyllistäminen tapahtuu helposti huomaamatta: alat vain vihjata toiselle, että jos hän olisi rohkeampi, hän voisi tehdä vaikka mitä, ja että nyt hän ei kykene siihen, mihin hän voisi kyetä. Yrität rohkaista häntä, tuloksetta. Ja kun yritätä rohkaista uudestaan ja uudestaan, alkaa toinen ahdistua, ja sinä alat turhautua. Silloin toinen alkaa kokea itsensä riittämättömäksi. Hän oppii, että riittämättömyyden tunne liittyy aina sinun läsnäoloosi ja ja alkaa karttaa sinua.

% haluat keskustella asioista. jos keskustelu menee pieleen, haluat keskustella siitä, miksi keskustelu meni pieleen. ja niin edelleen. keskusteluhalusi kaivelee toisten kipeitä kohtia, pakottaa heidät käsittelemään asioita, joihin he eivät ole valmiita.

Kaikki tämä voi johtaa siihen, että joudut paradoksaaliseen tilanteeseen: välität ympärillä olevista ihmisistä valtavasti ja teet kaikkesi heidän eteen. Asetat itsesi jopa vaaraan heidän vuokseen. Tästä huolimatta ihmiset alkavat työntää sinua pois. Pohdit, että missä on vika, eikä kukaan osaa kertoa sitä sinulle. “Olenko jollakin mystisellä tavalla kelvoton ihminen?” kysyt itseltäsi. Kukaan ei osaa vastata suruusi ja tuskaasi, jota tilanne sinussa herättää.

Pahimmillasi vaivut syvään itsesääliin ja muut alkavat karttaa sinua vielä entistäkin pahemmin. Uskallat näyttää kaiken surkeutesi ympärillesi, eikä kukaan kestä sitä. Äärimmäisessä tapauksessa tulet siihen tulokseen, että parasta, mitä voit tehdä läheisillesi, on tappaa itsesi.

Jos todella päädyt itsemurhaan—mikä toivonmukaan on tällaisissa tapauksissa hyvin harvinaista—on se todellisuudessa kaikkea muuta, kuin hyvä asia lähimmäisillesi. Kukaan ei kykene iloitsemaan kuolemastasi, vaikka eläessäsi aiheutitkin enemmän kuormaa kuin iloa; sen sijaan kuolemasi aiheuttaa vain tunnontuskia ympärillä oleville. Riittämättömyyden tunne vain kasvaa, kun ihmiset pohtivat, kuinka he olisivat voineet estää itsemurhasi, jos he olisivat ymmärtäneet sinua paremmin. Ihmiset kun todellisuudessa välittivät sinusta; he halusivat sinut kauaksi vain siksi, että läsnäolosi oli käytännössä liian kuormittavaa.

Itsesi murhaamisen sijaan sinun on saatava jostakin kivenkolosta itsellesi se puuttuva empatian palanen, jonka kautta ymmärtää pelokkuutta ja arkuutta. Se on sinulle erittäin vaikeaa, koska et tidä, miltä elämä tuntuu silloin, kun itseensä ei todella meinaa kyetä uskomaan. Mutta sinun on saatava tuo palanen, sillä ilman sitä olet käytännön tasolla vain egoistinen, ylimielinen kakara. Kaikki välittämisesi, rehellisyytesi, aitoutesi ja avoimuutesi valuu hukkaan, jos et osaa olla jyräämättä toisia ihmisiä allesi.

Voi olla, että joudut kokemaan kovia, ennenen kuin löydät puuttuvan palasen. Saatat tuhota suhteesi ihmiseen, jonka vuoksi voisit antaa henkesi. Sinua saatetaan alkaa vihata. Sinua saatetaan alkaa pitää narsistina.

Kun olet miettinyt tekosiasi tarpeeksi, saatat löytää tuon empatian palasen mikä sinulta on puuttunut. Kun niin käy, alat kokea syvää häpeää ja syyllisyyttä siitä, kuinka olet ymmärtämättömyyttäsi satuttanut toisia. Sinusta tuntuu pieneltä ja surkealta niiden ihmisten rinnalla, joita olet jyrännyt. Tällaiset tunteet ovat välttämättömiä; ne kuuluvat prosessiin. Ne vaihtavat ylimielisyytesi nöyryyteen ja antavat sinulle ne toisten kunnioituksen palaset, jotka sinulta ovat puuttuneet.

Sinusta voi tuntua siltä, ettet ansaitse niitä ihmisiä elämääsi, joita rakastat ja joita olet satuttanut. Mutta se, että koet näin, tarkoittaa sitä, että saatat vihdoin kyetä olemaan rakastamillesi ihmisille kokonaisvaltaisesti hyvä ihminen. Muut eivät näe sitä, ja he suhtautuvat sinuun edelleen negatiivisesti. Voi olla, että he eivät koskaan näe muutosta.

Velvollisuutesi on korjata sen minkä voit, ja hyväksyä toisten negatiivinen suhtautuminen sinuun. Olet itse rakentanut sen silloin, kun et vielä ymmärtänyt toisia kunnolla. Kannat menneisyyden typeryyden taakkaa harteillasi koko loppuelämäsi. Se on kohtalosi.

% Toisten ongelmien ratkominen
Halu keskustella asioista, joihin toiset eivät kykene
Totuudella ei ole mitään arvoa, jos ihminen ei kestä sitä tai kykene uskomaan sitä. Toisesta välittäminenkin voi olla käytännön tasolla täysin arvotonta—olipa se kuinka vahvaa ja syvää tahansa—jos hän ei sitä kykene ottamaan vastaan. Ihmiselle ei voi antaa liian suurta lahjaa.
“Mutta sehän on vain lahja toiselle, että näytän hänelle, miltä minusta tuntuu!” saatat ajatella. Ja näin asia onkin, Kuten vanha sananlasku sanoo: säännöt on tunnettava, jotta niitä voi rikkoa.
Ongelmasi ilmenevät sellaisten ihmisten kanssa, joista välität syvästi. Pärjäät loistavasti sinulle merkityksettömien ihmisten kanssa.

#### Jos uskot itseesi heikosti

### Sampon ja Miian tragedia

### Tästä kirjasta

Tämän kirjan tarina alkoi 15. marraskuuta 2018. Olin miettinyt kovasti erästä mönkään mennyttä ihmissuhdetta. Minulla on paljon kokemusta ihmissuhteessa epäonnistumisesta; niin paljon, että koen välillä olevani sen alan asiantuntija. Niinpä päätin kirjoittaa kirjan eri tavoista, joilla ihmissuhteen voi sössiä. Uhosin saavani kirjan valmiiksi tasan sadassa päivässä, jotta epäonnistumisesta aiheutuvan häpeän pelko ajaisi minua kirjoittamaan.

Prosessini kävi kuitenkin niin synkäksi ja syväksi, etten saanut moneen kymmeneen päivään kirjoitettua mitään kunnollista. Ajattelin olevani surkea ja kelvoton ihminen, enkä jaksanut välittää häpeästä, jonka epäonnistuminen saisi aikaan. Muistan olleeni jossakin vaiheessa niin surkeana, että olin aina hiljaa, jos paikalla oli minun lisäkseni kaksi muuta henkilöä. En uskonut, että sanomisillani olisi voinut olla mitään arvoa verrattuna heidän sanomisiinsa, ja niin jätin juttelun heidän tehtäväkseen.

Sitten tapahtui jotain. Oivalsin, etten sittenkään ole surkea ja kelvoton ihminen; että olin vain tehnyt vääriä tulkintoja ja etsinyt vastausta vääristä paikoista. Aloin ajatella, että saan kyllä kirjan kirjoitettua, tarvitsen vain enemmän aikaa. Kun sata päivää tuli täyteen 23. helmikuuta 2019, en julkaissut kirjaa, mutta julkaisin pienen lappusen, jossa ilmoitin tämän kirjan kirjoittamisesta. Lappunen oli samalla tämän kirjan ensimmäinen versio.

Julkaisen kirjasta uusia versioita sitä mukaa, kun kirjoitusprosessi etenee. Tämä on versio VersionNumDecimal, ja se on julkaistu DateSuomi. Uusin versio on luettavissa kirjan nettisivulla osoitteessa <BookURL>.

Prosessi pyörii ja pyörii, ja versionumeron kasvaessa olen oivaltanut lisää asioita. Olen luullut uskovani itseeni joka suhteessa vahvasti, ja välillä olen jopa syyttänyt itseuskoni vahvuutta kaikista kohtaamani ongelmista. Nyt olen huomannut, ettei asia olekaan niin. Olen esimerkiksi huomannut pelkääväni hylkäämistä. Tämä on ollut vaikea hoksata, sillä se aktivoituu omalla kohdallani vain tietynlaisissa tilanteissa. Lisäksi omassa historiassani ei ole oikeastaan mitään kovin traumaattista kokemusta, joka olisi vaikuttanut asiaan: ehkä pahin on, kun 14-vuotiaana olin valtavan ihastunut erääseen tyttöön, joka sitten yhtäkkiä tuntemattomasta syystä katkaisi kaiken yhteyden minuun. Mutta oli kokemuksia tai ei, saa hylkäämisen pelko minut käyttäytymään tietyissä tilanteissa tuhoisalla tavalla.

Ihmiset ovat usein haluttomia näkemään heikkouksia itseuskossansa. Itsekin olen ollut ihastunut ajatukseen itsestäni muita vahvemmin itseensä uskovana tyyppinä. Syy on pitkälti kulttuurissa: se sanoo, että heikko itseusko on heikkoutta. (Käytin itsekin samanlaista kielenkäyttöä, kun kirjoitin lukua 2. Pitää muuttaa sitä, kunhan kerkeän.) Mutta tämä on ilmiselvää valetta. Heikko itseusko ei tarkoita muuta, kuin että ihminen on tietämätön omasta vahvuudestaan.

Ja heikko itseusko on seurausta siitä, että maailma on tavalla tai toisella hakannut ihmistä. Se, ketä maailma hakkaa, on sattuman kauppaa; paljon riippuu siitä, mihin ihminen sattuu syntymään. On järjetöntä syyllistää ihmistä sellaisesta asiasta, mikä johtuu ympäröivän maailman satunnaisella tavalla suuntautuneesta väkivaltaisuudesta ja rakkaudettomuudesta.

Samalla kun opin itsestäni ja muista ihmisistä asioita, muuttuu myös tämän kirjan kirjoitusta ohjaava ajatus. Kirjasta ei olisi tullut mitään ilman viime aikojen oivalluksia, pohdin. Sitten tajuan, että ehkä minulta puuttuu edelleen suuria ja tärkeitä oivalluksia. Mikä minä keskenkasvuinen kakara olen kirjoittamaan mistään mitään?

Sitten tulen takaisin erääseen syvälliseen totuuteen, jonka olen tiennyt jo pitkään. Mikään ei ole koskaan valmista. Prosessi pyörii niin kauan, kunnes se kuolee pois, mutta valmiiksi se ei tule. Se, että kirja tai mikä tahansa asia on joskus valmis, on vain keinotekoinen valinta. Tämä kirja on valmis sitten, kun se on edennyt versioon 1.000 eli tuhannenteen versioonsa.

Ei tämän kirjan tarvitse olla koskaan missään syvällisessä mielessä valmis, ei edes sitten, kun versionumero on tasan yksi. Tästä tulee vain kokoelma hajanaisia ajatuksia, joka toivottavasti auttaa ihmisiä paikantamaan omia ja ymmärtämään ja hyväksymään toisten itseuskon heikkouksia. Haluan, että ihmiset uskoisivat itseensä vahvemmin.

#### Paperikopio

Voit tilata paperikopion tämän kirjan uusimmasta versiosta hintaan 25 euroa sisältäen postikulut Suomeen. Hintaan sisältyy toinen paperikopio kirjan uusimmasta versiosta valitsemanasi myöhempänä ajankohtana. Voit siis ostaa kopion aikaisessa vaiheessa ja saada samaan hintaan kopion valmiista versiosta, kunhan pääsen kirjoitusprosessissa sinne asti. Jos haluat tilata kirjan, ota yhteyttä minuun; puhelinnumeroni on AuthorPhoneNumber. Pidätän oikeudet muutoksiin.

Paperikopio on kauniisti ladottu, mutta vaatimaton itse printtaamani ja nitomani kirjanen, jossa ei ole tavallista paperia paksumpia kansia. Voi olla, että teen valmiista versiosta hienomman painoksen, jos kirja saa suosiota.

#### Tekniikasta

Joitakin lukijoita saattaa kiinnostaa, että olen toteuttanut kirjan tekniseti jokseenkin omalaatuisella tavalla. Lähdekoodi on yksi ajettava tiedosto (README.md), joka sisältää kirjan tekstin lisäksi muun muassa Pythonilla kirjoittamani yksinkertaisen Markdown-parserin. Tiedosto on samalla koko homman toimintaselostus. Se löytyy helposti luettavaksi muotoiltuna kirjan lähdekoodireposta osoitteesta <SourceURL>.

## Itsetunto

Kaikki ihmiset eivät ole hyviä. Maailmassa on jonkun verran räikeän sysipahoja ihmisiä, kuten sarjamurhaajia, ja hyvin paljon myrkyllisiä ihmisiä, joiden pahuus on vähemmän näkyvää, mutta kuitenkin täysin todellista ja hyvin tuhoisaa. Niinpä kysymykseen “Olenko minä hyvä ihminen?” ei ole itsestäänselvää vastausta. Tämä on perimmäinen syy sille, miksi ihmiset epäilevät itseänsä.

### Itsetunnon kerrokset

Ihmisen voi ajatella olevan kuin sipuli, jossa on monta kerrosta. On syvällisempiä osia ja pinnallisempia osia. Ihmisen itsetunto ei ole mikään suoraviivainen yhdellä numerolla mitattavissa oleva suure: siinä missä toinen uskoo ytimeensä ja epäilee pintaansa, pitää toinen ulkokuortansa mahtavana mutta pelkää luottaa sisimpäänsä. Kuinka vahvasti ihminen uskoo mihinkäkin kerrokseensa määrittää sen, millaisia valintoja hän rohkenee tehdä elämässään.

Itsetunnon heikkous ei koskaan ole ihmisen omaa syytä. Kaikki riippuu siitä, kuinka menestyksekkäästi maailma on onnistunut murskaamaan häntä; kuinka kattavasti myrkylliset voimat ovat päässeet uskottelemaan hänelle, että hän on huono ihminen. Vahvat ihmiset ovat onnekkaita yksilöitä, jotka ovat sattuneet kasvamaan rakastavassa ja turvallisessa ympäristössä ja välttyneet liian pahoilta iskuilta.

Aivan ihmisen ytimessä ovat hänen tarkoitusperänsä. Pyrinkö hyvään? Haluanko olla hyvä toisille ihmisille? Pidänkö toisia ihmisiä arvokkaina? Jos vastaat itsellesi mielesi täydellisessä yksityisyydessä *kyllä*, ilman että kukaan kuulee vastausta, olet pohjimmiltasi hyvä ihminen. Kaikki lähtee siitä.

Omaan perimmäiseen hyvyyteen uskominen ja toisten arvostaminen ovat yksi ja sama asia. Niillä ei ole mitään eroa. Ihminen, joka ei pidä toisia ihmisiä lähtökohtaisesti kallisarvoisina, ei usko itseensä. Myyttinen itsevarma kovanaama, joka katsoo muita alaspäin on yksi maailman kertomista valheellisista tarinoista.

Heti ytimen ulkopuolella ovat tunteet ja intuitio eli päätöksenteon syvällisimmät mekanismit. Luotanko sisäiseen ääneeni? Kuuntelenko sydäntäni? Uskallanko valitsenko sen, mikä tuntuu oikealta, vaikka joku saattaisi pahoittaa siitä mielensä?

Seuraavaksi tulevat kyvyt ymmärtää omaa ja muiden tunteita ja ajattelua sekä maailman toimintaa. Sitten tulee syvällisiä kykyjä, kuten kyky muotoilla ajatuksensa selkeiksi sanoiksi tai kyky ratkaista riitoja. Tässä vaiheessa mukaan sekoittuu elämän aikana omaksuttuja teorioita, uskomuksia ja ideologioita, kuten vaikkapa ihmisarvot.

Uloimmilla kerroksilla on yksinkertaisia taitoja, kuten polkupyörällä ajaminen tai laskutaito; tietoa, kuten vaikkapa maailmanhistorian tapahtumakulkuja; mielipiteitä, kuten “ananas kuuluu pitsaan;” ja aivan viimeisenä se, miltä ihminen näyttää ensisilmäyksellä ulospäin.

Syvemmällä olevat osat ovat pintakerroksia vakaampia: vaatteita ja kampausta voi vaihtaa vaikka useamman kerran päivässä, mutta sydämen vaihtaminen on lähes mahdottoman vaikea operaatio. Tämä tarkoittaa sitä, että myös usko omiin pintakerroksiin muuttuu helpommin ja nopeammin kuin usko ytimeen.

Niinpä kiusanteko ja lyttääminen vaurioittavat ensimmäisenä pintaa. Pinta myös elpyy nopeasti, ja mikäli sen alla on vahva sisus, ei satunnainen kiusanteko ole vaarallista. Mutta jos kiusanteko on rankkaa ja etevää, nakertaa se pikkuhiljaa myös sisempiä kerroksia, jotka eivät elvy nopeasti, vaikka kiusanteko lakkaisikin. Koska pinta elpyy sisusta nopeammin, näyttäytyy rajua kiusaamista menneisyydessään kokenut ihminen usein kovanaamana, jonka kuoren läpi ei meinaa nähdä millään.

Vahvat pintakerrokset mahdollistavat sujuvan sosiaalisen kanssakäymisen tavallisissa, helpoissa olosuhteissa ja menestyksekkään toiminnan yhteiskunnan rakenteissa, kuten kouluissa, työpaikoilla, harrastusyhteisöissä ja politiikassa. Ne mahdollistavat suuret määrät irtoseksiä, rahaa, julkisuutta, ihailua ja valtaa.

Syvemmällä olevat kerrokset määrittävät, mihin näistä tarkoitusperistä ihminen pintaansa käyttää. Vahva sisus tekee ihmisestä vähemmän riippuvaisen pintakerroksesta, mikä mahdollistaa ulompien kerrosten kriittisen tarkastelun ja kyseenalaistamisen sekä korjaamisen ja kehittämisen. Sisimmässään vahva ihminen pystyy ottamaan vastaan iskuja ja ymmärtämään niiden viestittämän kriittisen sanoman, mikä voi johtaa heikompaan itsetuntoon pintakerroksissa.

Vahva sisus saa ihmisen paradoksaalisesti näyttämään heikommalta. Tämä johtuu siitä, että vahva usko sisimpään rohkaisee ihmistä näyttämään ulompien kerrosten heikkouksia. Hyvin vahva ihminen uskaltaa näyttää heikkouksia hyvin läheltä ydintä ja saattaa pitää hyvännäköistä ulkokuorta jopa täysin turhana asiana.

Vahva usko sisimpään saa ihmisen näkemään toisen ihmisen pinnan läpi, ohi kulttuurillisen kerroksen, ja suhtautumaan toisen heikkouksiin lempeydellä. Se saa ihmisen näkemään syvällisen hyvyyden toisessa, jos sitä vain on olemassa. Se saa ihmisen myös tunnistamaan myrkylliset, pelkästä kuoresta muodostuvat epärehelliset ihmiset, ja tekee hänet immuuniksi heidän hyökkäyksilleen ja provokaatioilleen.

Ytimen ympäristössä vahva ihminen ei häpeä tai pelkää tunteitaan, ei edes häpeän tunnetta. Hän ajattelee, että jokainen tunne—olipa kyse rakkaudesta, vihasta, kierosta seksuaalisesta himosta, syyllisyyden tunteesta, itsesäälistä, vitutuksesta tai ihan mistä tahansa—on tärkeä ja merkityksellinen osa häntä, ja että jos tunne otettaisiin pois, häviäisi hänestä jotakin oleellista. Hän kokee, että hänessä ei ole mitään syvällisellä tavalla pahaa tai huonoa puolta, vaikka käytännössä tuleekin tehtyä virheitä ja toimittua typerästi.

Vahva ihminen ei aina hetkahda pienestä, mutta toisaalta joissakin tilanteissa vahvan ihmisen rohkeus kuunnella hiljaistakin sisäistä ääntä tekee hänestä hyvin herkän ja tunteellisen ihmisen. Se, että viisas ihminen olisi aina rauhallisessa “zen-tilassa” on puhdas myytti. Ainainen mielenrauha ei tarkoita muuta kuin sitä, että ihminen ei poistu koskaan mukavuusalueelta; että ihminen menee joka kerta siitä, missä aita on matalin. Oikeasti vahva ja viisas ihminen kärsii ja piehtaroi tunteissa aivan samalla tavalla kuin heikkokin ihminen, eikä hänen elämässään ole juuri mitään kadehdittavaa.

Vahvalle ihmiselle kaikki ihmiset—ehkä lukuunottamatta täysin tyhjiä kuoria—ovat aidosti melkoisen tasa-arvoisia ja yhdenvertaisia, sillä vahva ihminen näkee kuorikerrosten toiselle puolelle ihmisen ytimeen, ja siellä kaikki ovat suurin piirtein samanlaisia. Niinpä vahva ihminen ajattelee sen etua, jonka etu on kokonaisuutta katsoen parhaaksi kaikille.

Vahva ihminen uskaltaa asettaa itsensä vaaraan monella eri tavalla, ja tekee niin, mikäli se on parasta hänelle tärkeiden ihmisten kannalta. Hän saattaa asettaa oman terveytensä tai henkensä vaaraan. Hän ei piittaa maineestaan. Hän saattaa tehdä valintoja, jotka saavat toiset inhoamaan tai vihaamaan häntä tai nauramaan hänelle; valintoja, jotka saavat hänet näyttämään heikolta tai pahalta ihmiseltä. Äärimmäisissä tapauksissa hän tekee valintoja, jotka voivat saada toiset riistämään häneltä hengen.

Vahva ei ole heikkoa parempi missään syvällisessä mielessä. Hän vain uskoo sekä itsensä että muiden olevan parempia ihmisiä, kuin mitä heikko ihminen uskoo heidän olevan. Tämä saa vahvan ihmisen käytännössä tekemään enemmän hyvää. Heikon ihmisen vaikutus maailmaan paranee jos vain hänen uskonsa häneen itseensä vahvistuu.

Ei ole mitään järkeä uskoa itseensä, jos tavoitteena on päästä parempaan olotilaan. Itseensä uskominen tekee elämästä monella tavalla piinallista. Tarinoiden sankarit ovat kadehdittavia hahmoja vain siksi, että katsojat arvostavat heitä. Oikeassa elämässä katsojia ei ole paikalla.

### Epävarmuuden vaikutus

Itsetunto sanelee, mitä ihminen uskaltaa yrittää. Epävarmuudet vähentävät rohkeutta ja saavat ihmisen jättämään asioita tekemättä. Usein tämä on aivan paikallaan, sillä liika rohkeus tekee elämästä vaarallista ja saattaa johtaa tuhoisiin ratkaisuihin.

Suomen kaltaisessa kohtalaisen vapaassa ja maltillisessa yhteiskunnassa vahva itsetunto on vaarallista etupäässä silloin, kun se sijaitsee pintakerroksella. Jos esimerkiksi tavan tallaaja vailla kokemusta laskuvarjohypystä uskoisi kykenevänsä hyppäämään moottoripyörän ja laskuvarjon kanssa kapeaan jokeen, saattaisi hän innostuspäissään yrittää hyppyä ja kuolla pois. Joissakin kulttuureissa myös sisempien kuorien itsetunto on erittäin vaarallinen asia: maailmassa on paikkoja, joissa yksinkertaisen mielipiteen sanominen ääneen tai sorretun sanallinen puolustaminen voivat johtaa kidutukseen tai kuolmantuomioon.

Hyvin suuri osa ihmisten epävarmuudesta on tavalla lamauttavaa, maailman kertomiin valheisiin perustuvaa epävarmuutta. Epävarmuudet estävät yrittämästä ja siten haittaavat omien kykyjen testaamista ja harjoittelua ja uusien kykyjen oppimista ja ymmärryksen hankkimista. Mitä syvemmän kerroksen epävarmuudesta puhutaan, sitä laaja-alaisemmin se lamauttaa ihmisen kykyä omaksua uutta ja selvittää, kuinka oma mieli ja muu maailma toimii.

Vahvan itsetunnon vaarallisuus riippuu siitä, kuinka syvä ymmärrys ihmisten ja maailman toiminnasta sillä on parinaan. Viisaus kykenee estämään vaarallisen estämään vaarallisen teon, vaikka rohkeutta siihen olisi tarpeeksi. Kohtaamme tässä paradoksin: viisaaksi ei ole mahdollista kasvaa ilman rohkeutta mennä tuntemattomille vesille, ja rohkeus ilman viisautta on vaarallista. Turvallista polkua viisauteen ei ole olemassa.

Koska epävarmuudet estävät yrittämästä, estävät ne myös onnistumisen kokemusten saamista, ja siten hidastavat itsetunnon vahvistumista. Epävarmuus saa ihmisen myös tulkitsemaan lopputuloksen helpommin epäonnistumiseksi, joten epävarmuus voi jopa kasvattaa itseään pikku hiljaa.

Onko kyseessä onnistuminen vai epäonnistuminen riippuu siitä, minkä tason kerros asiaa tulkitsee. Syvemmät kerrokset näkevät positiivisia seikkoja tapahtumissa, jotka pintakerros haluaisi vain unohtaa. Siinä missä pinta haluaa nähdä välitöntö menestystä, arvostavat syvemmät kerrokset oppimista ja kehittymistä. Kaikesta on mahdollista oppia jotakin, ja niinpä sisältä vahva ihminen kokee tapahtuman suuremmalla todennäköisyydellä onnistumiseksi.

Kävin kerran erään naisen kanssa treffeillä kaupungin ulkopuolella. Yövyimme metsässä kahdenkymmenen asteen pakkasessa. Treffeistä ei syntynyt sen ihmeellisempää “juttua” joten pintapuolisesti katsottuna kyse oli epäonnistumisesta. Muistelen kuitenkin treffejä suurena menestyksenä. Tämä johtuu siitä, että koska tutustuimme helpon ja tylsän kahvilan tai baarin sijaan luonnossa, kohtalaisen haastavissa olosuhteissa, oli meillä ongelmia ratkottavana yhdessä, ja tutustuminen oli paljon kokonaisvaltaisempaa ja syvempää.

Treffien perimmäinen tarkoitus ei ole voittaa treffikumppania itselleen. Kyse ei ole valloituksesta. Todellisuudessa molemmat osapuolet ovat jo alusta lähtien samalla puolella, ja toimivat saman tavoitteen eteen: he ovat ottamassa selvää siitä, tulevatko he toimeen sillä tavalla, että pariutuminen olisi mahdollista. Treffit ovat onnistuneet, jos ne antavat tietoa tästä asiasta.

Eri kerrosten epävarmuuksilla on hyvin erilaisia vaikutuksia. Uloimpien kerrosten epävarmuus aiheuttaa hiljaisuutta, syrjään vetäytymistä ja korostetun korrektia mutta kuitenkin maltillista kohteliaisuutta. Pinnallisesti epävarma ihminen saattaa pelätä baarissa tanssimista, työhaastattelua tai koulutehtävän tekemistä luokan edessä.

Jos ihminen on epävarma vain pinnallisesti, kannattaa häntä yksinkertaisesti rohkaista yrittämään ja antaa ymmärtää, että epäonnistuminen ei haittaa. Jos ihminen on epävarma myös hiukan syvemmältä, voi rohkaiseminen olla turhaa, ja rohkaisevien sanojen toistaminen voi aiheuttaa paineita. Pahimmillaan rohkaiseminen aiheuttaa ahdistusta ja lukkiutumista.

Opiskellessani fysiikkaa ja matematiikkaa tehdyt laskuharjoitukset kirjattiin laskuharjoitustilaisuudessa paperille. Assistentti katsoi sitten paperista, kuka on tehnyt minkäkin tehtävän, ja pyysi jonkun taululle esittämään ratkaisunsa koko porukalle. Taululle meneminen pelotti minua niin paljon, että lopetin nopeasti tehtävien merkkaamisen paperille, huolimatta siitä, että silloin pisteet jäivät saamatta. En uskonut kykyyni tehdä tehtäviä oikein, saatikka kykyyni esittää ratkaisuni taululla.

Aloin tehdä tehtäviä vasta pitkän ajan kuluttua topologian kurssilla. Tämä johtui siitä, että koin henkilökohtaista yhteyttä kurssia pitävään opettajaan. Tunsin, kuinka hän arvosti matemaattista ajatteluani, ja niinpä rohkenin mennä taululle. Innostuin taulutyöskentelystä lopulta niin paljon, että aloin tehdä laskuharjoituksissa kaikki tehtävät, joita kukaan ei ollut merkannut tehdyiksi.

Syvemmällä sijaitsevat epävarmuudet aiheuttavat syvällisempiä vaikeuksia. Jos ihminen ei usko kykyynsä ajatella ja ymmärtää asioita, ei hän silloin ajattele kovin pitkälle eikä myöskään usko ajattelunsa lopputuloksia. Hän ei uskalla omata vahvoja mielipiteitä eikä sanoa ajatuksiaan ääneen; sen sijaan hän myötäilee toisten mielipiteitä ja ajatuksia loputtomiin. Hän saattaa olla “kaikkien puolella” “ymmärtää jokaista osapuolta” ja vaikuttaa liukkaalta, epärehelliseltä kameleontilta, joka pyrkii miellyttämään kaikkia.

Tällä tavalla epävarma ihminen on taipuvainen uskomaan auktoriteetteja ja altis propagandalle. Hän ei kyseenalaista kuulemiaan asioita ja maailma näyttäytyy hänelle sumeana mysteerinä, joka ei tottele minkäänlaista logiikkaa. Mainostajien on helppo myydä hänelle roskaa.

Jos ihminen ei usko omaan ajatteluunsa, on hänen kanssaan vaikea käydä keskustelua. Onnistunut keskustelu vaatii äärimmäistä maltillisuutta ja lempeyttä vahvemmalta osapuolelta. Hänen on varottava tiukasti perusteluja vaativaa sävyä, vältettävä näkemysten teilausta ja omien näkemysten jyräävää esittämistä. Hänen on tyydyttävä vähäiseen määrään varovaisia, mahdollisesti naiiveja ja pinnallisia näkemyksiä.

Jos keskustelu onnistuu, rohkenee arka ihminen ajattelemaan ja sanomaan asioita, joita hän ei tavallisesti uskaltaisi päästää ilmoille. Käy ilmi, että hän kykeneekin ajattelemaan. Vahvempi osapuoli vaikuttuu aremman henkilön ajattelusta, joka puolestaan kokee olevansa arvostettu ajattelunsa vuoksi. Arka ihminen muuttuu millimetrin verran vähemmän araksi.

Vielä syvemmällä sijaitseva tunteen ja intuition tason epävarmuus saa ihmisen käyttäytymään kuin tahdoton tuuliviiri, jonka suunta riippuu täysin ulkoisista voimista, jotka sitä riepottelevat. Tällainen ihminen ei meinaa erottaa omaa ääntään ulkopuolelta tulevien huutojen seasta. Hänellä on vaikeuksia tunnistaa epärehellisellä, pahantahtoisella agendalla lähestyviä ihmisiä, ja hänen on vaikea pistää kampoihin henkisesti haastavissa olosuhteissa.

Tällaisen ihmisen käytös voi joskus vaikuttaa pahantahtoiselta. Ehkä hän lähti kanssasi suhteeseen nopeasti, mutta jutusta ei syntynytkään rakkaustarinaa. Tilanne vaikuttaa siltä, että hän huijasi sinua ja pelasi tunteillasi, vaikka tosiasiassa hän ei vain tiennyt, mitä hän halusi. Hän lähti sinne suuntaan, mihin voimakkaampi ääni ohjasi, ja se sattui sillä hetkellä olemaan väärä suunta. Kun hän toistaa saman kaavan tulevaisuudessa, tilanne vaikuttaa siltä, että peli vain pyörii uutta kierrosta. Mutta taaskaan ei todellisuudessa ole kyse pelin pelaamisesta; hän vain heittelehtii jatkuvasti eri suuntiin sitä mukaa kun ympärilläolevat emotionaaliset voimat muuttuvat.

Tuuliviirin tavoin heittelehtivän ihmisen tukena oleminen voi olla hyvin vaikeaa. Hänelle on helppo syöttää aivan vahingossa omia näkemyksiä, ja näkemysten tuputtaminen voi tuntua jopa hyvältä idealta, kun toiselta näyttää puuttuvan oma tahto. Todellisuudessa tällaisella toiminnalla on kuitenkin kaikkia muuta kuin positiivinen vaikutus: se luo lisää melua ja hukuttaa hänen omaa ääntään entisestään. Voi olla, että hänen tahtonsa laimenee entisestään.

Se, mikä tuuliviiriä todellisuudessa voi auttaa, on että itse pitää mahdollisimman pientä meteliä ja arvostaa sitä pienenpientä tahdon kuiskausta, joka tuuliviiristä joskus kuuluu. Se on kaikista arvokkain asia. Silloin tuuliviiri huomaa itsekin oman äänensä paremmin ja hänen uskonsa siihen vahvistuu.

Hyvin lähellä ydintä sijaitseva epävarmuus näyttäytyy ihmisen syvällisenä epäilynä hänen omasta hyvyydestä. Tällainen ihminen voi tulkita esimerkiksi oman sisäisen äänen olemassaolon todisteeksi hänen itsekkyydestä. Hän voi pitää itseään narsistina ja perustella asiaa absurdilla teoretisoinnilla; esimerkiksi oman erityistaidon tai lahjakkuuden tiedostaminen tai mustasukkaisuuden, katkeruuden tai itsesäälin tunteen havaitseminen voi olla riittävä syy narsismin itsediagnosointiin. Hän voi olla varma, että hänessä on voimakas paha ja pimeä puoli, ja kokea siitä syyllisyyttä ja häpeää. Hän voi pitää “negatiivisten” tunteiden, kuten vihan, läsnäoloa todisteena tästä.

Tällainen ihminen tarvitsee ennenkaikkea ehdotonta hyväksyntää, lämpöä ja välittämisen osoittamista—*jos hän kykenee sen ottamaan vastaan.* Vastaanottokyvyn ylittävä rakkaus saa aikaan lähinnä pakokauhua. Hänen voi olla vaikea näyttää vastaanottokyvyn ylittymistä, mikä voi helposti johtaa siihen, että hänestä kovasti välittävä ihminen antaa vahingossa rakkautta liikaa. Tiedän tämän virheen omakohtaisesti erittäin hyvin. Tiedän, kuinka pahasti täysin vilpitön välittäminen voi mennä pieleen.

Aivan ytimessä sijaitseva epävarmuus on pahinta kaikista. Jos ihminen ei usko omaan hyvyyteensä, ei hän silloin osaa pyrkiä hyvään, ja hän on käytännössä aivan oikeasti huono asia toisille ihmisille. Pahimmillaan hän voi olla jopa suorastaan paha ihminen, joka kylvää valheita ja tuhoa ympärilleen. Tällainen ihminen ei äkkiseltään näytä ollenkaan epävarmalta, sillä hänen koko näennäinen ihmisyytensä perustuu esitettyihin rooleihin, jotka pyrkivät näyttäytymään edukseen. Palaan tähän aiheeseen erittäin seikkaperäisesti kirjan myöhemmässä vaiheessa.

Kaikki ytimen ympäristössä sijaitsevat epävarmuudet tekevät avun vastaanottamisesta hankalaa, sillä ne saavat ihmisen kokemaan itsensä käytännön tasolla arvottomaksi ihmiseksi. Jos ihminen kokee olevansa käytännössä arvoton, ei hän voi ottaa hyvällä omallatunnolla apua vastaan. “Jos tuo laupias auttaja vain ymmärtäisi, millainen surkimus minä olen…”

Avun vastaanottaminen voi ahdistaa myös sen takia, että siihen liittyy vaara velan muodostumisesta. Auttaja voi olla täysin pyyteettömällä asenteella matkassa, mutta sitä avun vastaanottaja ei välttämättä voi uskoa. Itseasiassa arvottomaksi itsensä kokeva ihminen voi tulkita tarjotun avun jopa osoitukseksi siitä, että auttaja on pahantahtoisella asenteella liikkeellä: “Eihän kukaan tällaista surkimusta oikeasti haluaisi auttaa. Kyllä tähän täytyy olla koira haudattuna.”

### Suojamekanismeja

Epävarmuudet saavat ihmisen pelkäämään kerrostensa paljastamista katseille ja kosketukselle. Syy tähän on ilmeinen: jos ihminen uskoo jonkun tärkeän osansa olevan jossain mielessä huono, on hänen silloin järkevää pelätä toisten suhtautuvan tavalla tai toisella negatiivisesti—syrjien, pilkaten, lyöden tai hyläten—jos tuo hänen kelvottomaksi kokemansa osa paljastuu muille. Ihmisillä on paljon erilaisia tapoja suojauta kerroksiaan paljastumiselta.

Yksinkertaisin tapa on tietysti jättää asia tekemättä. Tämä onnistuu helpoiten pinnallisten asioiden kohdalla. Minulla oli joskus tapana antaa rahani ja ostokseni kaverille juuri ennen kaupan kassaa ja antaa hänen hoitaa kaupankäynnin, sillä pelkäsin jollakin kummalla tavalla mokaavani ostotapahtuman. Sittemmin olen huomannut, että mokaaminen ei ole kovin todennäköistä. Se ei myöskään ole vaarallista, jos niin sattuu käymään.

Jos pelko kohdistuu johonkin pinnalliseen sosiaaliseen tilanteeseen, kuten useamman ihmisen edessä puhumiseen, on pelon ylittämiseen olemassa yksinkertainen keino: pelosta kertominen. Käytän tätä itse melkein aina kun koen vähääkään ylimääräistä jännitystä. Temppu toimii uskomattoman hyvin: se sulattaa jännityksen pois ja tekee itse asian selostamisesta helppoa ja luontevaa. Se osoittaa kuulijoille, että olen tavallinen ihminen, jolla on heikkoutensa. Se poistaa ylimääräisen muodollisuuden tilanteesta.

Toki jännityksestä kertominen on kiusallista ja epäcoolia. Se onkin koko homman idea: kun paljastaa kerta heitolla, että on kaikkea muuta kun cool ja smooth tyyppi, katoavat paineet virheettömästä esiintymisestä välittömästi taivaan tuuliin. Sitten on paljon helpompi keskittyä siihen, mitä oikeasti haluaa tilanteessa sanoa. Samalla muidenkin paineet pienenevät. Kaikki voittavat.

Pelkän pintakerroksen epävarmuudet eivät ole niin vakava asia, sillä pintakerroksella ei ole paljoa väliä, jos sen alla on vankka sisus. Mitä syvemmän kerroksen epävarmuudesta puhutaan, sitä voimakkaammat suojamekanismit aktivoituvat.

Yleisesti ottaen syvällisempi epävarmuus saa ihmisen korostamaan pintakerroksia toimiessaan. Hän näyttää muille vain uloimmat osansa. Ulkokerros pelaa monesti jonkinlaista roolia, ja rooli saattaa vaihdella usein. Tällainen ihminen voi vaikuttaa epärehelliseltä pelurilta.

### Panssari

### Kun vahva ja heikko kohtaavat

## Kaksi tietä aikuisuuteen

Ihmislapsi ei syntyessään ole juuri mitään valmista. Se ei ole hyvä eikä paha, eikä se usko mihinkään muuhun kuin välittömässä läheisyydessä olevien aikuisten lämpöön. Se ei osaa muuta kuin huutaa, hengittää ja syödä. Potentiaalia siinä on kuitenkin valtavasti. Se, mitä kaikkea lapsesta on mahdollista tulla, lukee sen DNA-molekyylissä muutaman miljardin emäsparin digitaalisena merkkijonona eli genomina. Sen voi nykypäivänä ladata omalle tietokoneelleen Internetistä ainakin osoitteesta <http://www.ensembl.org/Homo_sapiens/Info/Index>.

Jos tarkkoja ollaan, linkin takaa ei löydy kenenkään yksittäisen ihmisen genomia, vaan useamman ihmisen genomeista koostettu referenssigenomi. Yksittäisten ihmisten genomit poikkeavat referenssigenomista ehkä vajaan prosentin verran. Niinpä jokaisessa lapsessa on oma uniikki potentiaalinsa. Mutta erot ovat pieniä, ja monesti on käytännöllistä ajatella, että kaikki vastasyntyneet ihmislapset ovat täysin samanlaisia. Muiden lajien jälkeläiset—kuten vaikkapa simpanssin, jonka genomi poikkeaa ihmisen genomista neljän prosentin verran—ovatkin sitten jo merkittävästi erilaisia lapsia. Ne eivät kykene oppimaan läheskään kaikkea sitä, mitä aikuinen ihminen voi osata. Mutta toisaalta ne voivat oppia esimerkiksi kiipeilemään puissa paljon paremmin.

Genomin kieli on vaikeaselkoinen, luonnonlakien ja kemian määrittelemä kieli, jota kukaan ei ymmärrä kunnolla. Sitä ei myöskään ole kukaan kirjoittanut; sen sijaan luonto on noin neljän miljardin vuoden aikana kokeillut valtavan määrän erilaisia genomeja ja jalostanut niistä yhä vain toimivampia yksilöitä. Jokainen eletty elämä on koe, joka tutkii jonkin satunnaisesti jo olemassa olevien genomien pohjalta muodostuneen uuden genomin kelpoisuutta, ja jonka tuloksen määrittää se, pääsikö genomista kasvanut otus jatkamaan sukua vai ei. Jokaisella elävällä eläimellä on omassa sukuhistoriassaan takana vain ja ainoastaan menestyksekkäitä edeltäjiä.

Menestyksekkäiden genomien ja niistä kasvavien eläinyksilöiden toimintaa voisi hämmästellä loputtomiin. Tärkeintä on ymmärtää, että joka ikisellä genomin määrittämällä potentiaalisella ominaisuudella on jokin merkityksellinen rooli eläimen toiminnassa. Tämä pätee myös ihmiseen: voimme olla varmoja, että kateus on tärkeä tunne, vaikka sen merkitys ei heti putkahtaisikaan mieleen. Kaikki tunteet—nekin, joita kutsumme negatiivisiksi tunteiksi—ovat tärkeitä, eikä ihminen enää menestyisi, jos jokin niistä otettaisiin pois.

Minkä tahansa eläimen toiminnan ymmärtäminen onnistuu ainakin periaatteessa maalaisjärkeä käyttäen, kunhan vain oivaltaa, että sen jokainen piirre on ratkaisu johonkin ongelmaan, jonka se on toistuvasti kohdannut siinä elinympäristössä, missä se on kehittynyt. Ihmisen tapauksessa tuo elinympäristö on kivikautinen heimoyhteiskunta, joka ei tuntenut tulta, vaatteita ja kivityökaluja hienostuneempaa teknologiaa. Nykyaikainen ihmisten maailma on lajikehityksen mittakaavassa ollut olemassa vain pienen hetken. Nykyihmisen kohtaamat erikoiset ongelmat, kuten vaikkapa ylipaino tai pähderiippuvuus, johtuvat yksinomaan siitä, että ihmiset elävät itselleen vieraassa maailmassa.

### Luonto ja kasvatus

Ihmisen genomi sisältää tiedon siitä, kuinka oppia kävelemään, puhumaan, järkeilemään, ilmeilemään, rakastamaan, vihaamaan, myötäelämään toisten kokemuksia ja niin edelleen. Oppiminen taas vaatii harjoittelua: yrityksiä, erehdyksiä ja sopivanlaista palautetta. Genomi siis kertoo, kuinka ihmisyyden kerrokset rakentuvat toinen toisensa päälle, *jos* kasvuolosuhteet ovat suotuisat. Jos olosuhteet ovat jotain muuta, häiriintyy kerrosten rakentuminen. Pimeässä tynnyrissä kasvanut lapsi ei kykene aikuisena mihinkään, ei edes kävelemään tai katsomaan.

Kun pieni lapsi itkee, se kysyy: “Olenko minä hyvä?” Ja kun äiti ottaa sen syliinsä, pussaa sitä ja antaa sille maitoa, hän vastaa: “Kyllä olet!” Nämä varhaiset vuorovaikutukset kasvattavat lapsen uskoa tämän ytimeen. Samalla ytimestä tulee totta, sillä aivan ihmisen keskustassa, siellä missä perimmäiset tarkoitusperät sijaitsevat, hyvyys ja usko omaan hyvyyteen ovat yksi ja sama asia.

Samalla kun ydin vahvistuu, alkaa sen ympärille kasvaa muita kuoria. Usko ytimeen saa lapsen kokeilemaan uusia asioita epäonnistumista pelkäämättä. Lapsi harjoittelee liikkumista, puhumista, sosiaalisia taitoja ja toisten ymmärtämistä. Se harjoittelee ympäröivän maailman jäsentämistä, järkeilyä, tulevaisuuden ennustamista ja suunnitelmien tekemistä. Vanhemmat osallistuvat prosessiin kannustamalla lasta.

Miksi viisivuotiaan lapsen piirros on hänen vanhempiensa mielestä hieno? Se ei varmasti ole sitä samalla tavalla, kuin jonkun kuuluisan kuvataitelijan teokset saattavat olla. Lapsen piirros on hieno, koska lapsi päästää siinä sisäisen äänensä valloilleen. Hän kertoo piirroksella jotain itsestään, siitä miten hän ajattelee ja tuntee. Jos vanhemmat eivät pidä piirroksesta, tarkoittaa se lapselle sitä, että hänessä voi olla jotakin syvällisesti pielessä.

Lapsi tekee joskus virheitä, ja niinpä hän tarvitsee kehujen lisäksi myös toruja. Jos kaikki menee hyvin, osuu toruminen lähinnä uloimmille kuorille eikä koskaan ytimeen. Pintaosat kärsivät pientä osumaa ja itku pääsee, mutta lapsen ydin korjaa ulommat osat entistä vahvemmiksi. Tällä tavoin lapsen kaikki kerrokset kasvavat ytimestä lähtien vahvaksi kokonaisuudeksi.

Jotta kasvuprosessi onnistuisi, on vanhempien *pakko* rakastaa lasta. Lapsi ei rohkene harjoittelemaan ytimen ympärillä olevilla kuorilla, ellei se usko itseensä ytimessä, ja se taas ei onnistu, ellei jokin ulkopuolinen luotettava taho vala tuota uskoa siihen.

!MainTextEnds!

!EOC!

python3 build.py $1 $2 $3 $4
